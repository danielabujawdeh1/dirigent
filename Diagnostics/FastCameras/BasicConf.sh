source /dev/shm/golem/Commons.sh

diag_id=FastCameras
setup_id="BasicConf"
whoami="Diagnostics/$diag_id/$setup_id"

DAS=""

Devices="Video/PhotronMiniUX50-a/Radial Video/PhotronMiniUX50-b/Vertical Computers/PhotronCamerasPC/FastCameras"

function DefineTable()
{
    echo OK;    
#    CreateTable discharge.position_stabilization
}


function WakeOnLan(){ 
    echo "*B1OS7H"|telnet 192.168.2.254 10001 1>&- 2>&-; # 
 } 

function SleepOnLan(){ 
    echo "*B1OS7L"|telnet 192.168.2.254 10001 1>&- 2>&-; # 
}

function OpenSession()
{
    echo OK;    
#    InitFunctGen 
}


function Arming()
{
    echo OK;    
}

function GetReadyTheDischarge ()
{
    echo OK;
    #GeneralTableUpdateAtDischargeBeginning diagnostics.$diag_id #@Commons.sh
}




function PostDischargeAnalysis
{
    #convert -resize $icon_size SpeedCamera.png analysis.jpg
    sleep 25
    mkdir Camera_Radial
    mkdir Camera_Vertical
    cp /mnt/share/`ls -1tr /mnt/share/|grep C002|tail -1`/*.* Camera_Radial/
    cp /mnt/share/`ls -1tr /mnt/share/|grep C001|tail -1`/*.* Camera_Vertical/
    
    sed -i "s/shot_no=0/shot_no\ =\ `cat $SHMS/shot_no`/g" Camera_Position.ipynb 
    jupyter-nbconvert --ExecutePreprocessor.timeout=60 --to html --execute Camera_Position.ipynb --output analysis.html > >(tee -a jup-nb_stdout.log) 2> >(tee -a jup-nb_stderr.log >&2)
    convert -resize $icon_size icon-fig.png graph.png
    
    
    GenerateDiagWWWs $diag_id $setup_id ../Diagnostics/FastCameras # @Commons.sh
}



