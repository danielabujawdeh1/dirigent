BASEDIR="../.."
source $BASEDIR/Commons.sh

diag_id=FastExCameras
setup_id="FastExCameras"
whoami="Diagnostics/$diag_id/$setup_id"

DAS=""
Devices=""

function InitCheck()
{
    echo -ne Check Exilim Cameras ...
    if [ `ssh Discharge "echo q | exf1ctrl|grep Error|wc -l"` -eq 1 ]; then LogIt "Camera check  ...KO, problem"; critical_error "Exilim camera KO";exit 1; fi
    echo OK
 
}

function Arming()
{
    ssh Discharge "rm plasma_detected icon.png plasma_film.png"
    ssh Discharge "echo 'movie under progress' > movie_progress;nohup python3 /home/golem/Drivers/FastCameraExF1CASIO/capture_mosaic.py plasma_film.png 1>/dev/null 2>/dev/null &" 
    
}


function WebRecord()
{
    echo $1 >> analysis.html
}

function Web()
{
    rm analysis.html
    WebRecord "<html><body><h1>Fast cameras</h1>"
    WebRecord "<a href='plasma_film.png'><img src='plasma_film.png' width='60%'/></a>"
    WebRecord "</body></html>"
    
}    



function PostDischargeAnalysis
{
    Web
    GenerateWWWs
    timeout=60

#while [ ! -f /dev/shm/golem/RASPs/Discharge/plasma_detected ]
while ssh Discharge "! test -s /home/golem/plasma_detected";
do
  # When the timeout is equal to zero, show an error and leave the loop.
  if [ "$timeout" == 0 ]; then
    echo "ERROR: Timeout while waiting for the file plasma_detected."
    return 1
  fi

  sleep 1
  echo $timeout s to wait for plasma_detected
  ((timeout--))
done
    scp Discharge:plasma_detected .
    if [ `cat plasma_detected` == "1" ]; then
        scp Discharge:plasma_film.png .
        #convert -resize !x150 plasma_film.png icon.png
        convert -resize 200x150 plasma_film.png rawdata.jpg
        cp rawdata.jpg graph.png
        Web
    else
        convert -size 200x150 xc:white -font "FreeMono" -pointsize 35 -fill black -annotate +15+15 "\nNo\nPLASMA\ndetected" rawdata.png
        
    fi
    #jupyter-nbconvert  --ExecutePreprocessor.timeout=30 --execute FastCameras.ipynb --output analysis.html

    return
    sed -i "s/shot_no\ =\ 0/shot_no\ =\ `cat /dev/shm/golem/shot_no`/g" FastCameras.ipynb
    scp FastCameras.ipynb golem@Abacus:FastCameras/ # Uprava4Abacus
    
    
    #jupyter-nbconvert  --ExecutePreprocessor.timeout=30 --execute notebook.ipynb --output analysis.html #Lokalni vypocet
    ssh golem@Abacus "cd FastCameras;/home/golem/anaconda3/bin/jupyter  nbconvert --ExecutePreprocessor.timeout=300 --execute FastCameras.ipynb --output analysis.html"
    
    scp golem@Abacus:FastCameras/analysis.html .
    scp golem@Abacus:FastCameras/SpeedCamera.png .
    convert -resize $icon_size SpeedCamera.png analysis.jpg

}

function GenerateWWWs
{

      GenerateDiagWWWs $diag_id $setup_id ../Diagnostics/FastExCameras # @Commons.sh

}



