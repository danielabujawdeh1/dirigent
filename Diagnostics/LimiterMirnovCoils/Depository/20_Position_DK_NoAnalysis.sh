#!/bin/bash

source /dev/shm/golem/Commons.sh


diag_id=LimiterMirnovCoils
setup_id="20_Position_DK_NoAnalysis"
whoami="Diagnostics/$diag_id/$setup_id"

DAS="DASs/NIstandard/20_Position_DK"

Devices="$DAS"

LastChannelToAcq=9
diags=('U_mc1' 'U_mc5' 'U_mc9' 'U_mc13' 'U_saddle' 'U_inquadr' 'U_mcout' 'U_currentclamp')

function DefineDiagTable()
{
    CreateDiagnosticsTable $diag_id
    AddColumnToDiagnosticsTable $diag_id vacuum_shot integer
    psql -c "COMMENT ON COLUMN  diagnostics.$diag_id.vacuum_shot IS 'Vacuum shot definition'" -q -U golem golem_database
    
}


function GetReadyTheDischarge ()
{

    GeneralDiagnosticsTableUpdateAtDischargeBeginning $diag_id #@Commons.sh
}


function PostDischargeAnalysis
{

    GeneralDAScommunication $DAS RawDataAcquiring
    ln -s ../../Devices/`dirname $DAS/` DAS_raw_data_dir

    for i in `seq 2 $LastChannelToAcq` ; do
       awk '{print $1","'\$$i'}' DAS_raw_data_dir/NIdata_6133.lvm > ${diags[$i-2]}.csv; 

    done 
    Analysis

      GenerateDiagWWWs $diag_id $setup_id `dirname $DAS` # @Commons.sh
      
}      


function NoAnalysis()
{
    cp $SW_dir/Management/imgs/Commons/WithoutAnalysis.png icon-fig.png
    convert -resize $icon_size icon-fig.png graph.png
}


function Analysis()
{

local nb_id="20_Position_DK"

   #export SHOT_NO=`cat ../../shot_no` #linux only
    sed -i "s/shot_no\ =\ 0/shot_no\ =\ `cat $SHMS/shot_no`/g" $nb_id.ipynb
    sed -i "s/vacuum_shot\ =\ 0/vacuum_shot\ =\ `cat /dev/shm/golem/ActualShot/Diagnostics/LimiterMirnovCoils/Parameters/vacuum_shot`/g" $nb_id.ipynb
    
    jupyter-nbconvert --ExecutePreprocessor.timeout=300 --to html --execute $nb_id.ipynb --output analysis.html > >(tee -a jup-nb_stdout.log) 2> >(tee -a jup-nb_stderr.log >&2)
    

    if [[ -f icon-fig.png ]]; then
        convert -resize $icon_size icon-fig.png graph.png
        convert -resize $icon_size icon-fig.png analysis.jpg
    fi
    


}


function Analysis_Ab()
{

local nb_id="20_Position_DK"

   #export SHOT_NO=`cat ../../shot_no` #linux only
    sed -i "s/shot_no\ =\ 0/shot_no\ =\ `cat $SHMS/shot_no`/g" $nb_id.ipynb
    sed -i "s/vacuum_shot\ =\ 0/vacuum_shot\ =\ `cat /dev/shm/golem/ActualShot/Diagnostics/LimiterMirnovCoils/Parameters/vacuum_shot`/g" $nb_id.ipynb
    scp $nb_id.ipynb golem@Abacus:Stabilizace/ # Uprava4Abacus
    
    
    #jupyter-nbconvert  --ExecutePreprocessor.timeout=30 --execute notebook.ipynb --output analysis.html #Lokalni vypocet
    ssh golem@Abacus "cd Stabilizace;mkdir -p Results;/home/golem/anaconda3/bin/jupyter  nbconvert --ExecutePreprocessor.timeout=300 --to html --execute $nb_id.ipynb --output analysis.html > >(tee -a jup-nb_stdout.log) 2> >(tee -a jup-nb_stderr.log >&2)"
    
    scp golem@Abacus:Stabilizace/analysis.html .
    scp golem@Abacus:Stabilizace/icon-fig.png .
    scp golem@Abacus:Stabilizace/plasma_position.png .
    scp golem@Abacus:Stabilizace/video.mp4 .
    scp golem@Abacus:Stabilizace/plasma_position.csv .
    scp golem@Abacus:Stabilizace/jup-nb_stderr.log .
    scp golem@Abacus:Stabilizace/jup-nb_stdout.log .
    scp golem@Abacus:Stabilizace/Results/*.* Results/
    scp golem@Abacus:Stabilizace/Results/* Results/

    if [[ -f icon-fig.png ]]; then
        convert -resize $icon_size icon-fig.png graph.png
        convert -resize $icon_size icon-fig.png analysis.jpg
    fi
    


}
