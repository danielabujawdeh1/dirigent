#!/bin/bash

SHM="/dev/shm/golem"
source $SHM/Commons.sh
#rPi=192.168.2.92 ## rPI IP adress 
shot_no=`CurrentShotDataBaseQuerry shot_no`

diag_id="TimepixDetector"
setup_id="TimepixDetector2xCdTe@RASP"
whoami="Diagnostics/$diag_id/$setup_id"

Devices="Radiation/AdvaPix_CdTe-a/device Radiation/AdvaPix_CdTe-b/device Computers/RasPi4-c/TimepixDetector Computers/RasPi4-b/TimepixDetector "

#account="pi@RasPi4-c"
#Rasps="RasPi4-c RasPi4-b"
Rasps="RasPi4-b"
function Arming()
{
    for rPi in $Rasps; do 
        echo Doing $rPi
        ssh pi@$rPi "rm /home/pi/data/*"
        ssh pi@$rPi "python3 armingCdTe.py $shot_no > /dev/null 2>/dev/null &"
    done
}




function PostDischargeAnalysis()
{
      sleep 5
        for rPi in $Rasps; do 
        echo "PostDischargeAnalysis @ $rPi"
              mkdir -p $rPi
              scp pi@$rPi:data/* $rPi/
              sed -i "s/shot_no\ =\ 0/shot_no\ =\ `cat /dev/shm/golem/shot_no`/g" $rPi.ipynb
              jupyter-nbconvert  --ExecutePreprocessor.timeout=20 --to html --execute $rPi.ipynb --output $rPi.html         > >(tee -a jup-nb_stdout-$rPi.log) 2> >(tee -a jup-nb_stderr-$rPi.log >&2)
              convert -resize $icon_size icon-fig.png $rPi.png

        done

        cp RasPi4-b.png graph.png
        HtmlGenerat

}

function HtmlGenerat()
{
SHOT_NO=`cat $SHM/shot_no`
echo "" > diagrow_$setup_id.html
echo "<tr>
    <td valign=bottom><img src=Diagnostics/TimepixDetector/name.png  width='100'/>&nbsp;</td>
    <td></td>
    <td valign=bottom><img src=Devices/Radiation/AdvaPix_CdTe-a/device.png  width='200'/>&nbsp;</td>
    <td valign=bottom><img src=Diagnostics/TimepixDetector/RasPi4-b.png  width='200'/>&nbsp;</td>
    <td valign=bottom><a href=Diagnostics/TimepixDetector/ title=Directory><img src=http://golem.fjfi.cvut.cz/_static/direct.png  width='30px'/></a></td>
    </tr>" >> diagrow_$setup_id.html
}

function HtmlGenerat_zmb()
{
    <td valign=bottom><img src=Diagnostics/TimepixDetector/RasPi4-c.png  width='200'/>&nbsp;</td>
}

