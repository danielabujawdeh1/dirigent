#!/bin/bash

SHM="/dev/shm/golem"
source $SHM/Commons.sh
#rPi=192.168.2.92 ## rPI IP adress 
shot_no=`CurrentShotDataBaseQuerry shot_no`

diag_id="TimepixDetector/TimePix3-Si-Nikhef-a"
setup_id="TimePix3-Si-Nikhef-a"
whoami="Diagnostics/$diag_id/$setup_id"

DAS="Computers/TimePix3-Nikhef/TimepixDetector"
Devices="$DAS"

function GetReadyTheDischarge()
{
ssh tpx3@TimePix3-Nikhef "source anaconda3/bin/activate;./Projects/Tpx3_DAQ/Tpx3_init_bash"
}

function Arming()
{
ssh tpx3@TimePix3-Nikhef "source anaconda3/bin/activate;./Projects/Tpx3_DAQ/Tpx3_run_bash" &
}



function PostDischargeAnalysis()
{
    ssh tpx3@TimePix3-Nikhef "source anaconda3/bin/activate; ./Projects/Tpx3_DAQ/Tpx3_analysis_bash"
    convert -resize $icon_size icon-fig.png graph.png
    GenerateDiagWWWs $diag_id $setup_id `dirname $DAS` $GooglePhotosPath # @Commons.sh

}

