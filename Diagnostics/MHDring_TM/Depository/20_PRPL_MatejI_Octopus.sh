#!/bin/bash

 
source /dev/shm/golem/Commons.sh

diag_id=MHDring_TM
setup_id="20_PRPL_MatejI"
whoami="Diagnostics/$diag_id/$setup_id"

DAS="DASs/NIoctopus/20_PRPL_MatejI"

Devices="$DAS"



function DefineDiagTable()
{
    CreateDiagnosticsTable $diag_id
    
}


function GetReadyTheDischarge ()
{

    GeneralDiagnosticsTableUpdateAtDischargeBeginning $diag_id #@Commons.sh
}



function PostDischargeAnalysis()
{
    GeneralDAScommunication $DAS RawDataAcquiring
     
    ln -s ../../Devices/`dirname $DAS/` DAS_raw_data_dir
    
    for i in `seq 9 24`; do awk '{print $1","'\$$i'}' DAS_raw_data_dir/NIdata.lvm > ring_$((i-8)).csv; done;

   GenerateDiagWWWs $diag_id $setup_id `dirname $DAS` Zrc6YenW4gfLxAjJ8 # @Commons.sh
    Analysis
}


function Analysis
{

#Workarround
    echo "<HTML><HEAD><TITLE></TITLE><BODY>
    <H1>Diagnostics analysis is not ready yet</H1>
    <!-- <img src="icon-fig.png"></img>-->
    <br></br></BODY></HTML>" > analysis.html
    cp $SW_dir/Management/imgs/Commons/ScriptNotReadyYet.png icon-fig.png
#Finals      
    convert -resize $icon_size icon-fig.png graph.png
}

