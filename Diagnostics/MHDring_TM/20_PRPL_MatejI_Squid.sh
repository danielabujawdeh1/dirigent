#!/bin/bash

 
source /dev/shm/golem/Commons.sh

diag_id=MHDring_TM
setup_id="20_PRPL_MatejI_Squid"
whoami="Diagnostics/$diag_id/$setup_id"
GooglePhotosPath=Zrc6YenW4gfLxAjJ8

DAS="DASs/NIsquid/20_PRPL_MatejI"

Devices="$DAS"



function DefineDiagTable()
{
    CreateDiagnosticsTable $diag_id
    
}


function GetReadyTheDischarge ()
{

    GeneralDiagnosticsTableUpdateAtDischargeBeginning $diag_id #@Commons.sh
}



function PostDischargeAnalysis()
{
    GeneralDAScommunication $DAS RawDataAcquiring
     
    ln -s ../../Devices/`dirname $DAS/` DAS_raw_data_dir
    
    for i in `seq 2 17`; do awk '{print $1","'\$$i'}' DAS_raw_data_dir/NIdata.lvm > ring_$((i-1)).csv; done;

   GenerateDiagWWWs $diag_id $setup_id `dirname $DAS` $GooglePhotosPath # @Commons.sh
    NoAnalysis
}


function NoAnalysis
{
   cp $SW_dir/Management/imgs/Commons/WithoutAnalysis.png icon-fig.png
    convert -resize $icon_size icon-fig.png graph.png

}

