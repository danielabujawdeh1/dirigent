BASEDIR="../.."
source $BASEDIR/Commons.sh


iag_id=HXRprobes
setup_id="21_RunAways_JCetal"
whoami="Diagnostics/$diag_id/$setup_id"

DAS="Oscilloscopes/TektrMSO58-a/21_RunAways_JCetal"


Devices="$DASs"


HXR_probes="CeBr_a YAP_a NaITl_a ZnSAg_a"
#(x,y,z, theta, phi, HV_PS@NIMcrate, HVchannel@NIMcrate, Voltage)
S_CeBr_a=(380 130 72 180 0 A 0 -700)
S_YAP_a=(380 130 72 180 0 A 1 -800)
S_NaITl_a=(380 130 72 180 0 A 2 -700)
S_ZnSAg_a=(380 130 72 180 0 A 3 -700)


columns="2 3 4 5 8 11"
diags=('U_Loop-b' 'CeBr_a' 'YAP_a' 'NaITl_a' 'Cosi' 'Trigger')


function PostDischargeAnalysis()
{
    #GeneralDAScommunication $DAS RawDataAcquiring
    ln -s ../../Devices/`dirname $DAS/` DAS_raw_data_dir
    
    #n=0;for i in $columns; do tail -q -n +13  DAS_raw_data_dir/TektrMSO56_ALL.csv |awk -F "," '{print $1","'\$$i'}'> ${diags[$n]}.csv;((n+=1)); done;
  
    export SHOT_NO=`cat ../../shot_no` # for linux
    sed -i "s/shot_no\ =\ 0/shot_no\ =\ `cat /dev/shm/golem/shot_no`/g" StandardDAS.ipynb
    #jupyter-nbconvert --execute StandardDAS.ipynb --to html --output analysis.html > >(tee -a jup-nb_stdout.log) 2> >(tee -a jup-nb_stderr.log >&2)
    NoAnalysis
    convert -resize $icon_size icon-fig.png graph.png
#    convert -resize $icon_size icon-fig.png $SHM/ActualShot/Diagnostics/BasicDiagnostics/analysis.jpg
#    convert $SHMS/Management/imgs/DAS_icon.jpg graph.png +append icon_.png
#    convert -bordercolor Black -border 2x2 icon_.png icon.png
    
    GenerateDiagWWWs $diag_id $setup_id `dirname $DAS` Diagnostics/Basic Sx48Muj5rcQZVGTLA
# @Commons.sh
    #GenerateWWWs
}

function NoAnalysis()
{
    cp $SW_dir/Management/imgs/Commons/WithoutAnalysis.png icon-fig.png
    convert -resize $icon_size icon-fig.png graph.png
}


function Analysis()
{

:

}