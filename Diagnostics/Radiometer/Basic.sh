#!/bin/bash

source ../../Commons.sh

diag_id="Radiometer"
setup_id="Basic"
whoami="Diagnostics/$diag_id/$setup_id"

DAS="DASs/Papouch-Ji/Radiometer"

Devices="$DAS"


LastChannelToAcq=12
diags=('Uloop' 'rm1' 'rm2' 'rm3' 'rm4'  'rm5' 'rm6' 'rm7' 'rm8'  'rm9' 'rm10' 'rm11')



function WakeOnLan()
{
    sleep 1;echo "*B1OS3H"|telnet 192.168.2.254 10001 #N wall
}


function SleepOnLan()
{
    sleep 1;echo "*B1OS3L"|telnet 192.168.2.254 10001
}


function PostDischargeAnalysis() 
{

    GeneralDAScommunication $DAS RawDataAcquiring $LastChannelToAcq
    ln -s ../../Devices/`dirname $DAS/` DAS_raw_data_dir

    for i in `seq 1 $LastChannelToAcq` ; do
        cp DAS_raw_data_dir/ch$i.csv ${diags[$i-1]}.csv
    done    

    GenerateDiagWWWs $diag_id $setup_id `dirname $DAS` $GooglePhotosPath # @Commons.sh
    NoAnalysis
}




function NoAnalysis
{
    :
    cp $SW_dir/Management/imgs/Commons/WithoutAnalysis.png icon-fig.png
    convert -resize $icon_size icon-fig.png graph.png

}
