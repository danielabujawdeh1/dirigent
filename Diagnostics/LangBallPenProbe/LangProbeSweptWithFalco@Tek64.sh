#!/bin/bash

source /dev/shm/golem/Commons.sh


diag_id="LangBallPenProbe"
setup_id="LangProbeSweptWithFalco@Tek64"
whoami="Diagnostics/$diag_id/$setup_id"

DAS="Oscilloscopes/TektrMSO64-a/LangProbeSweptWithFalco"



#pwd
source Universals.sh

#With FG:
Devices="RelayBoards/Energenie_LANpower-a/PowerSupply4DASs FunctionGenerators/RigolDG1032Z-a/FG4ElstatProbes $DAS"



columns="2 3 4 7 10"
diags=('U_bias' 'U_current' 'U_loop-d' 'U_probe' 'I_probe')


function GetReadyTheDischarge ()
{

   GeneralDiagnosticsTableUpdateAtDischargeBeginning $diag_id #@Commons.sh
}


function PostDischargeAnalysis() 
{

    GeneralDAScommunication $DAS RawDataAcquiring
    ln -s ../../Devices/`dirname $DAS/` DAS_raw_data_dir

    n=0;for i in $columns; do tail -q -n +13  DAS_raw_data_dir/TektrMSO64_ALL.csv |awk -F "," '{print $1","'\$$i'}'> ${diags[$n]}.csv;((n+=1)); done;
    

    GenerateDiagWWWs $diag_id $setup_id `dirname $DAS` # @Commons.sh
    Analysis
}


function Analysis
{
sleep 5
 gnuplot  -e  "\
set terminal png size 1600, 1000;unset xtics;set datafile separator ',';set bmargin 0;set tmargin 1;set lmargin 20;set rmargin 3;unset xlabel;set multiplot layout 6,1\
title 'GOLEM Shot #$SHOT_NO \@ `cat $SHM0/Production/Parameters/gas`, f_{fg}=`cat $SHM0/Diagnostics/$diag_id/Parameters/f_fg` Hz, u_{fg}=`cat $SHM0/Diagnostics/$diag_id/Parameters/u_fg` V , r^{LP}=`cat $SHM0/Diagnostics/$diag_id/Parameters/r_lp_tip` mm  ';\
set xrange [0:0.025];set yrange [-5:20];set style data dots;set ylabel 'U_{loop}-d [V]';plot 'U_loop-d.csv' u 1:2 t '' w l lc 1 ;\
set bmargin 0;set tmargin 0;unset title;\
set yrange [*:*];set ylabel 'U@_{fl}^{BPP} [V]';plot 'U_BPP_fl.csv'  u 1:2  w l lc 2 title '';\
set yrange [*:*];set ylabel 'U@^{LP}_{bias}^ [V]';plot 'U_bias.csv'  u 1:(\$2*-1)  w l lc 2 title '';\
set xtics;set ylabel 'I@^{LP}_{probe} [mA]';plot 'I_probe.csv'  u 1:(\$2*1000)  w l lc 3 title '';\
set xrange [*:*];set xtics; set ylabel 'I@^{LP}_{probe} [mA]';set xlabel 'U@^{LP}_{probe} [V]';plot '< paste -d "," U_probe.csv I_probe.csv' u 2:(\$4*1000)  w l lc 4 title 'VA char'\
" > icon-fig.png
    
    
    echo "<h1>Kick-off analysis for fast ion temp meas.</h1>
    <img src="icon-fig.png" width="80%"></img><br></br>"> analysis.html

    convert -resize $icon_size icon-fig.png graph.png
}



