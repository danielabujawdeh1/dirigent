SHM="/dev/shm/golem"
source $SHM/Commons.sh
source $SHM/Tools.sh

OMNI_HOME=/opt/OmniDriver/OOI_HOME
serial_number="HR+C1886"  # to select correct spectrometer in case there are several
acquisition_time=30000    # [us] total time over which to acquire spectra after trigger
integration_time=1670     # [us] integration time of fast spectra acquisition, 1.67 ms is the fastest
usbid="2457:1016"

function InitCheck()
{
    echo -ne Check Ocean Optics spectrometer
    if ! (lsusb | grep $usbid &>/dev/null) ; then
        LogIt "Ocean Optics Spectrometer check  ...KO, problem"
        critical_error "Ocean Optics spectrometer KO"
        exit 1
    fi
    echo OK
}

function Arming()
{
    # compile the source code into a highspeedacquisitionsample/Highspeedacquisitionsample.class
    # -d . tells it to compile the package into the current directory
    javac -classpath $OMNI_HOME/\* -d . HighSpeedAcquisitionSample.java
    mkdir -p data   # create a folder for data if it does not exist
    java -Djava.library.path=$OMNI_HOME -classpath $OMNI_HOME/\*:. highspeedacquisitionsample.HighSpeedAcquisitionSample $serial_number $acquisition_time $integration_time
    echo "OK"
}


function WebRecord()
{
    echo $1 >> analysis.html
}

function Web()
{
    rm analysis.html
    WebRecord "<html><body><h1>Ocean Optics spectrometer</h1>"
    WebRecord "<a href='data/'>Data folder</a>"
    WebRecord "</body></html>"
}



function PostDischargeFinals()
{
	jupyter nbconvert basic_spectra_analysis.ipynb --to html --execute
}



