BASEDIR="../.."
source $BASEDIR/Commons.sh



diag_id="RailProbe"
setup_id="BallPenProbeFluctPot@Tek58"
whoami="Diagnostics/$diag_id/$setup_id"

DAS="Oscilloscopes/TektrMSO58-a/BallPenProbeFluctPot"


Devices="$DAS"


columns="1 7"
diags=('U_loop-b' 'null' 'null' 'null' 'null' 'null' 'U_BPP_fl')



function GetReadyTheDischarge ()
{
  GeneralDiagnosticsTableUpdateAtDischargeBeginning $diag_id #@Commons.sh
}

function PostDischargeAnalysis_Tek64() 
{

    GeneralDAScommunication $DAS RawDataAcquiring
    ln -s ../../Devices/`dirname $DAS/` DAS_raw_data_dir

    n=0;for i in $columns; do tail -q -n +13  DAS_raw_data_dir/TektrMSO64_ALL.csv |awk -F "," '{print $1","'\$$i'}'> ${diags[$n]}.csv;((n+=1)); done;
    

    GenerateDiagWWWs $diag_id $setup_id `dirname $DAS` # @Commons.sh
    NoAnalysis
}

function PostDischargeAnalysis()
{
  
   sleep 4
    GeneralDAScommunication $DAS RawDataAcquiring
    ln -s ../../Devices/`dirname $DAS/` DAS_raw_data_dir_Tek58

    
    for i in $columns ; do
        cp DAS_raw_data_dir_Tek58/ch$i.csv ${diags[$i-1]}.csv
    done    

    GenerateDiagWWWs $diag_id $setup_id `dirname $DAS` # @Commons.sh
    #NoAnalysis
}

function NoAnalysis()
{
    cp $SW_dir/Management/imgs/Commons/WithoutAnalysis.png icon-fig.png
    convert -resize $icon_size icon-fig.png graph.png
}


function Analysis()
{

:

}
