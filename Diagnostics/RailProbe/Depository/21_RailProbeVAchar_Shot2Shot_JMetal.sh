#!/bin/bash

source ../../Commons.sh


diag_id=RailProbe
setup_id="21_RailProbeVAchar_Shot2Shot_JMetal"
whoami="Diagnostics/$diag_id/$setup_id"
GooglePhotosPath="Q5QjWRMeP3QMJRXX9"

DAS="DASs/Papouch-Za/21_RailProbe_JMetal_remote"


source Universals.sh

#With FG:
#Devices="RelayBoards/Energenie_LANpower-a/PowerSupply4DASs FunctionGenerators/RigolDG1032Z-a/FG4ElstatProbes $DAS"
#Devices="FunctionGenerators/RigolDG1032Z-a/FG4ElstatProbes $DAS"

#Without FG:
Devices="RelayBoards/Energenie_LANpower-a/PowerSupply4DASs PowerSupplies/GWInstekPSW-b/RailProbe $DAS"


LastChannelToAcq=4
diags=('I_RP' 'I_LP' 'U_fl_BPP' 'Null')
diags=('I_RP' 'I_LP' 'U_fl_BPP' 'Null')


function DefineDiagTable()
{
    CreateDiagnosticsTable $diag_id
    
}

function GetReadyTheDischarge ()
{
GeneralDiagnosticsTableUpdateAtDischargeBeginning $diag_id #@Commons.sh
}


function PostDischargeAnalysis() 
{

    GeneralDAScommunication $DAS RawDataAcquiring $LastChannelToAcq
    ln -s ../../Devices/`dirname $DAS/` DAS_raw_data_dir

    for i in `seq 1 $LastChannelToAcq` ; do
        cp DAS_raw_data_dir/ch$i.csv ${diags[$i-1]}.csv
    done    

    GenerateDiagWWWs $diag_id $setup_id `dirname $DAS` $GooglePhotosPath # @Commons.sh
    Analysis
}

    #GenerateDiagWWWs $diag_id $setup_id $DAS # @Commons.sh

function Analysis
{
    sed -i "s/shot_no\ =\ 0/shot_no\ =\ `cat ../../shot_no`/g" `basename $whoami`.ipynb
    jupyter-nbconvert  --ExecutePreprocessor.timeout=60  --to html --execute `basename $whoami`.ipynb --output analysis.html > >(tee -a jup-nb_stdout.log) 2> >(tee -a jup-nb_stderr.log >&2) #Lokalni vypocet
    convert -resize 200x!180 icon-fig.png graph.png
}


function NoAnalysis
{
    :
    cp $SW_dir/Management/imgs/Commons/WithoutAnalysis.png icon-fig.png
    convert -resize $icon_size icon-fig.png graph.png

}


