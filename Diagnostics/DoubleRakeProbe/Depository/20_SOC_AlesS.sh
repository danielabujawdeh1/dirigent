#!/bin/bash

BASEDIR="../.."
source $BASEDIR/Commons.sh


DASs="Papouch-Za"
DASsetup="DoubleRakeProbe"
namesize=100
iconsize=200


Drivers="PapouchDAS1210"



function PingCheck() { 
    CommonPingAllDevices  # @Commons.sh
}

function PrepareSessionEnv() {
    CommonPrepareSessionIssues # @Commons.sh
     
}

function Arming()
{
     GeneralDAScommunication  $SHM0 DASArming
}


function PrepareDischargeEnv() {
    CommonPrepareDischargeIssues # @Commons.sh
}

function OpenSession() {
     GeneralDAScommunication $SHMS DASOpenSession;
}


function SleepOnLan()
{
#    ssh abacus "sudo /sbin/shutdown now"
echo OK
}

function PostDischargeAnalysis()
{
     GeneralDAScommunication $SHM0 RawDataAcquiring
     
    ln -s ../../DASs/$DASs/ DAS_raw_data_dir

     convert $SHM0/DASs/$DASs/graph.png rawdata.jpg
    #convert -resize $icon_size $BASEDIR/analysis_wave_i/InnerStabilization/icon-fig.png analysis.jpg # to se nestiha ..
    # cp $BASEDIR/DASs/$ThisDev/das.jpg . #zatim ne
    GenerateWWWs
}


function GenerateWWWs
{
#echo '<HTML><META HTTP-EQUIV="Refresh" CONTENT="0;URL=
#http://golem.fjfi.cvut.cz/shots/'`cat $BASEDIR/shot_no`'XXXYYY/analysis.html"><HEAD><TITLE></TITLE><BODY></BODY></HTML>' > analysis.html


    Analysis


    echo '<HTML><HEAD><TITLE></TITLE><BODY>
    <H1>Raw data</H1>
    <img src="http://golem.fjfi.cvut.cz/shots/'`cat $BASEDIR/shot_no`'/DASs/$ThisDev/graph1.png"></img><br></br>
    <a href="http://golem.fjfi.cvut.cz/shots/'`cat $BASEDIR/shot_no`'/DASs/$ThisDev/">Data directory</a>
    
    </BODY></HTML>' > rawdata.html
    
    echo '
    <H2>On Stage Diagnostics: SOC Ales Socha  </H2>
    <img src="Diagnostics/DoubleRakeProbe/icon-fig.png"></img><br></br>
    <a href="http://golem.fjfi.cvut.cz/shots/'`cat $BASEDIR/shot_no`'/DASs/XXYY">Data</a><br></br>
    ' > onstage.html
    
    
    echo "<tr>
    <td valign=bottom><a href=http://golem.fjfi.cvut.cz/shots/$SHOT_NO/Diagnostics/$DASsetup/><img src=http://golem.fjfi.cvut.cz/shots/$SHOT_NO/Diagnostics/$DASsetup/name.png  width='$namesize'/></a></td>
    <td valign=bottom><a href=http://golem.fjfi.cvut.cz/shots/$SHOT_NO/Diagnostics/$DASsetup/setup.html><img src=http://golem.fjfi.cvut.cz/shots/$SHOT_NO/Diagnostics/$DASsetup/setup.png  width='$iconsize'/></a></td>
    <td valign=bottom><a href=http://golem.fjfi.cvut.cz/shots/$SHOT_NO/DASs/$DASs/analysis.html><img src=http://golem.fjfi.cvut.cz/shots/$SHOT_NO/DASs/$DASs/das.jpg  width='$iconsize'/></a></td>
    <td valign=bottom><a href=http://golem.fjfi.cvut.cz/shots/$SHOT_NO/DASs/$DASs/><img src=http://golem.fjfi.cvut.cz/shots/$SHOT_NO/DASs/$DASs/icon.png  width='$iconsize'/></a></td>
    <td valign=bottom><a href=http://golem.fjfi.cvut.cz/shots/$SHOT_NO/Diagnostics/$DASsetup/analysis.html><img src=http://golem.fjfi.cvut.cz/shots/$SHOT_NO/Diagnostics/$DASsetup/analysis.jpg  width='$iconsize'/></a></td>
    <td valign=bottom><a href=http://golem.fjfi.cvut.cz/shots/$SHOT_NO/Diagnostics/$DASsetup/><img src=http://golem.fjfi.cvut.cz/_static/direct.png  width='100px'/></a></td>
    <td valign=bottom><a href=http://golem.fjfi.cvut.cz/shots/$SHOT_NO/Diagnostics/$DASsetup/DataProcessing/DataProcessing.html><img src=http://golem.fjfi.cvut.cz/_static/DataProc.png  width='100px'/></a></td></tr></tbody></table>" > diagrow.html;
    

    
    

}

function Analysis
{
    #jupyter-nbconvert  --ExecutePreprocessor.timeout=30 --execute PetiProbe.ipynb --output analysis.html


#    sed -i "s/shot_no\ =\ 0/shot_no\ =\ `cat /dev/shm/golem/shot_no`/g" PetiProbe.ipynb
#    scp PetiProbe.ipynb golem@Abacus:PetiProbe/ # Uprava4Abacus
    
    
    jupyter-nbconvert  --ExecutePreprocessor.timeout=30 --execute notebook.ipynb --output analysis.html #Lokalni vypocet
    #ssh golem@Abacus "cd PetiProbe;/home/golem/anaconda3/bin/jupyter  nbconvert --ExecutePreprocessor.timeout=300 --execute PetiProbe.ipynb --output analysis.html"
    
    #scp golem@Abacus:PetiProbe/analysis.html .
    #scp golem@Abacus:PetiProbe/icon-fig.jpg .
    convert -resize $icon_size icon-fig.png analysis.jpg
}
