#!/bin/bash

diag_id=DoubleRakeProbe
setup_id="Basic"

whoami="Diagnostics/$diag_id/$setup_id"
DAS="DASs/Papouch-Za/DoubleRakeProbe_Basic"

source Universals.sh


#Devices="RelayBoards/Energenie_LANpower-a/PowerSupply4DASs $DAS"
Devices="$DAS"


LastChannelToAcq=12
#LastChannelToAcq=2
# (1,2,3,4,5,6,10,11,12,13,14) Oznaceni kabelů z DRP
diags=('DRP-R1' 'DRP-R2' 'DRP-R3' 'DRP-R4' 'DRP-R5' 'DRP-R6'  'DRP-L1' 'DRP-L2' 'DRP-L3' 'DRP-L4' 'DRP-L5' 'DRP-L6')


function GetReadyTheDischarge ()
{
    export PGPASSWORD='rabijosille';psql -c "INSERT INTO diagnostics.${diag_id,,} (shot_no, session_mission, start_timestamp, setup_id, das, comment, X_discharge_command) VALUES ($SHOT_NO,'`cat $SHM/session_setup_name`','`date "$date_format"`','$setup_id','`dirname $DAS`','`cat $SHMP/comment`','`cat $SHMP/CommandLine`')" -q -U golem golem_database
    
    local WhatToDo=`cat $SHMP/diagnostics_${diag_id,,}`
    if [[ -f $SHMP/diagnostics_${diag_id,,} ]]; then
        psql -c "UPDATE  diagnostics.${diag_id,,} SET $WhatToDo WHERE shot_no IN(SELECT max(shot_no) FROM diagnostics.${diag_id,,})" -q -U golem golem_database
    fi
    
    mkdir -p $SHM0/Diagnostics/$diag_id/Parameters
    #psql -c "\COPY  diagnostics.${diag_id,,}  TO '$SHM0/Diagnostics/$diag_id/Parameters.csv' With CSV DELIMITER ',' HEADER" -q -U golem golem_database;
    export PGPASSWORD='rabijosille';psql -c "SELECT * FROM  diagnostics.${diag_id,,}  WHERE shot_no IN(SELECT max(shot_no) FROM diagnostics.${diag_id,,})" -x -q -U golem golem_database > $SHM0/Diagnostics/$diag_id/AllParameters;
    grep -v RECORD $SHM0/Diagnostics/$diag_id/AllParameters|awk -F "|" '{print "echo "$2" > '$SHM0'/Diagnostics/'$diag_id'/Parameters/"$1}' -|bash;
}



function PostDischargeAnalysis() 
{

    GeneralDAScommunication $DAS RawDataAcquiring $LastChannelToAcq
    ln -s ../../Devices/`dirname $DAS/` DAS_raw_data_dir

    for i in `seq 1 $LastChannelToAcq` ; do
        cp DAS_raw_data_dir/ch$i.csv ${diags[$i-1]}.csv
    done    

    NoAnalysis
    GenerateDiagWWWs $diag_id $setup_id `dirname $DAS` # @Commons.sh
    
}

function NoAnalysis
{
    cp $SW_dir/Management/imgs/Commons/WithoutAnalysis.png icon-fig.png
    convert -resize $icon_size icon-fig.png graph.png

}


function Analysis
{
    sed -i "s/shot_no\ =\ 0/shot_no\ =\ `cat ../../shot_no`/g" `basename $whoami`.ipynb
    jupyter-nbconvert  --ExecutePreprocessor.timeout=30  --to html --execute `basename $whoami`.ipynb --output analysis.html > >(tee -a jup-nb_stdout.log) 2> >(tee -a jup-nb_stderr.log >&2) #Lokalni vypocet
    convert -resize $icon_size icon-fig.png graph.png
}

#===========================SW Tuning=========================================

# cd /dev/shm/golem/ActualShot/Diagnostics/DoubleRakeProbe/;cp /golem/svoboda/Dirigent/Diagnostics/DoubleRakeProbe/*.* .;source ../../Commons.sh;source 21_DRP_KHetal.sh; TestIt
TestIt()
{
cp /golem/svoboda/Dirigent/Devices/`dirname $DAS`/*.* /dev/shm/golem/ActualShot/Devices/`dirname $DAS`/;
TriggerManagement GetReadyTheDischarge
GeneralDAScommunication $DAS Arming
sleep 3
TriggerManagement Trigger
sleep 1
TriggerManagement SecurePostDischargeState
PostDischargeAnalysis;
ll /dev/shm/golem/ActualShot/Devices/`dirname $DAS`/; 
ll
}

