#!/bin/bash

source /dev/shm/golem/Commons.sh


diag_id=PetiProbe
setup_id="21_PetiProbe_JAetal"
whoami="Diagnostics/$diag_id/$setup_id"

DAS="Oscilloscopes/TektrMSO58-a/21_PetiProbe_JAetal"



#pwd
source Universals.sh

#With FG:
Devices="RelayBoards/Energenie_LANpower-a/PowerSupply4DASs FunctionGenerators/RigolDG1032Z-a/FG4ElstatProbes $DAS"



LastChannelToAcq=5
diags=('U_loop-b' 'U_bias' 'U_current' 'U_fl_LP' 'U_fl_BPP')


function GetReadyTheDischarge ()
{

   GeneralDiagnosticsTableUpdateAtDischargeBeginning $diag_id #@Commons.sh
}


function PostDischargeAnalysis() 
{

    GeneralDAScommunication $DAS RawDataAcquiring
    ln -s ../../Devices/`dirname $DAS/` DAS_raw_data_dir

    for i in `seq 1 $LastChannelToAcq` ; do
        cp DAS_raw_data_dir/ch$i.csv ${diags[$i-1]}.csv
    done    

    GenerateDiagWWWs $diag_id $setup_id `dirname $DAS` # @Commons.sh
    Analysis
}


function Analysis
{
    gnuplot  -e  "set terminal png size 1600, 1000;unset xtics;set datafile separator ',';set bmargin 0;set tmargin 1;set lmargin 20;set rmargin 3;unset xlabel;set multiplot layout 5, 1 title 'GOLEM Shot #$SHOT_NO, f_fg=`cat $SHM0/Diagnostics/PetiProbe/Parameters/f_fg` Hz, u_fg=`cat $SHM0/Diagnostics/PetiProbe/Parameters/u_fg` V';set xrange [*:*];set yrange [-5:20];set style data dots;set ylabel 'U_loop [V]';plot 'U_loop-b.csv' u 1:2 t '' w l lc 1 ;set bmargin 0;set tmargin 0;unset title;set xtics;set yrange [*:*];set ylabel 'U_bias [V]';plot 'U_bias.csv'  u 1:2  w l lc 2 title '';unset xtics;set ylabel 'U_res [V]';plot 'U_current.csv'  u 1:2  w l lc 3 title '';set xtics; set ylabel 'U_current/R_i [I]';set xlabel 'U_bias [V]';plot '< paste -d "," U_bias.csv U_current.csv' u 2:(\$4)/47  w l lc 4 title 'R_i=47 Ohm'" > icon-fig.png
    
    echo "<h1>Kick-off analysis for fast ion temp meas.</h1>
    <img src="icon-fig.png" width="80%"></img><br></br>"> analysis.html

    convert -resize $icon_size icon-fig.png graph.png
}



