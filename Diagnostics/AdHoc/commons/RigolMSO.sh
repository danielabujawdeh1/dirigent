# -b,-d,-e
# for address in 11:22 19:45 14:7b;do sudo arp-scan --interface=global0 147.32.4.0/24|grep $address;sudo arp-scan --interface=global0 147.32.5.0/24|grep $address; done

function CommonPostDischargeAnalysis()
{
    GeneralDAScommunication $DAS RawDataAcquiring
    ln -s ../../Devices/`dirname $DAS/` DAS_raw_data_dir-$line
    NoAnalysis
    #convert -resize $icon_size icon-fig-$line.png graph-$line.png
    convert -resize $icon_size icon-fig.png graph.png    
    GenerateDiagWWWs $diag_id $setup_id `dirname $DAS` "Nan" "Nan" 
}

function NoAnalysis()
{
    #cp $SW_dir/Management/imgs/Commons/WithoutAnalysis.png icon-fig-$line.png
    cp $SW_dir/Management/imgs/Commons/WithoutAnalysis.png icon-fig.png
}

function Analysis()
{
    sed "s/shot_no\ =\ 0/shot_no\ =\ `cat /dev/shm/golem/shot_no`/g" commons/StandardDAS.ipynb > StandardDAS-$line.ipynb
    sed -i "s/Rigol_line\ =\ 'xy'/Rigol_line\ =\ \'$line\'/g" StandardDAS-$line.ipynb
    jupyter-nbconvert --execute StandardDAS-$line.ipynb --to html --output analysis-$line.html > >(tee -a jup-nb_stdout.log) 2> >(tee -a jup-nb_stderr.log >&2)

}


function CommonOpenSession()
{
    SCALE=10
    OFFSET=0
    echo "
    :CHANnel1:DISPlay ON;CHANnel1:PROBe 1;CHANnel1:SCALe 3;CHANnel1:OFFSet -10;
    :CHANnel2:DISPlay ON;CHANnel2:PROBe 1;CHANnel2:SCALe $SCALE;CHANnel2:OFFSet $OFFSET;
    :CHANnel3:DISPlay ON;CHANnel3:PROBe 1;CHANnel3:SCALe $SCALE;CHANnel3:OFFSet $OFFSET;
    :CHANnel4:DISPlay ON;CHANnel4:PROBe 1;CHANnel4:SCALe $SCALE;CHANnel4:OFFSet $OFFSET;
    :TIMebase:DELay:ENABle OFF;TIMebase:MAIN:SCALe 0.002;TIMebase:MAIN:OFFSet 0.008;
    :TRIGger:EDGE:SOURce CHANnel1;TRIGger:EDGE:SLOPe POS;TRIGger:EDGE:LEVel 5;
    :STOP;:CLEAR;
    :SYSTem:KEY:PRESs MOFF"|$COMMAND
    echo OK
}


function CommonGetData()
{
#SETUP
    mkdir -p ScopeSetup;quer='XINC XOR XREF START STOP'; echo `for i in $quer; do echo :WAV:$i?";";done`'*IDN?'|nc -w 1 RemoteTrainingOsc-b.golem 5555|sed 's/;/\n/g'|sed \$d > ScopeSetup/answers;for i in $quer; do echo _WAV_$i;done > ScopeSetup/querries;paste ScopeSetup/querries ScopeSetup/answers > ScopeSetup/ScopeSetup; 
    #cat ScopeSetup/ScopeSetup;
    awk '{print "echo " $2 " >ScopeSetup/"$1}' ScopeSetup/ScopeSetup |bash;

#TIME
    _WAV_XINC=`cat ScopeSetup/_WAV_XINC`;_WAV_START=`cat ScopeSetup/_WAV_START`;_WAV_STOP=`cat ScopeSetup/_WAV_STOP`; perl -e '$Time=0;for ( $i='$_WAV_START' ; $i <= '$_WAV_STOP'; $i++ ){printf "%.5e\n", $Time;$Time=$Time+'$_WAV_XINC'}' > Time
    
#DATA
    for CHANNEL in `seq 1 4`; do 
	echo "$ThisDev oscilloscope:  Downloading data for channel $CHANNEL"
	echo ":WAV:SOURCE CHAN$CHANNEL;:WAV:FORM ASCii;:WAV:MODE NORM" |$scope_address;sleep 0.5s;echo ":WAV:DATA?" |$scope_address|cut -c 12-|sed 's/,/\n/g'|tee data|gnuplot -e 'set terminal jpeg;plot "<cat" w l' > ${diags[$CHANNEL]}.jpg;
	paste -d, Time data > ${diags[$CHANNEL]}.csv
    done
    rm Time data
#    paste data* > data_all
}


