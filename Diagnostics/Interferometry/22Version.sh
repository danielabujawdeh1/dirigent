#!/bin/bash

 
source ../../Commons.sh

diag_id=Interferometry
setup_id="22Version"
whoami="Diagnostics/$diag_id/$setup_id"

DAS="Oscilloscopes/TektrDPO3014-a/Interferometry"

Devices="$DAS"



function DefineDiagTable()
{
    CreateTable diagnostics.$diag_id
    
}


function GetReadyTheDischarge ()
{

    GeneralDiagnosticsTableUpdateAtDischargeBeginning $diag_id #@Commons.sh
}



function PostDischargeAnalysis()
{
    GeneralDAScommunication $DAS RawDataAcquiring
     
    ln -s ../../Devices/`dirname $DAS/` DAS_raw_data_dir


   GenerateDiagWWWs $diag_id $setup_id `dirname $DAS` Zrc6YenW4gfLxAjJ8 # @Commons.sh
   
    if [ `cat $SHM0/Production/Parameters/DataProcessing` == "off" ] ; then
        No_Analysis
    else
        Analysis
    fi

}


function Analysis
{
local nb_id="phase_detection_density"

    sed -i "s/shot_no\ =\ 0/shot_no\ =\ `cat $SHMS/shot_no`/g" $nb_id.ipynb
    jupyter-nbconvert --execute $nb_id.ipynb --to html --output analysis.html

    convert -resize $icon_size icon-fig.png graph.png
}

function No_Analysis
{

#Workarround
    echo "<HTML><HEAD><TITLE></TITLE><BODY>
    <H1>Diagnostics analysis is not ready yet</H1>
    <!-- <img src="icon-fig.png"></img>-->
    <br></br></BODY></HTML>" > analysis.html
    cp $SW_dir/Management/imgs/Commons/WithoutAnalysis.png icon-fig.png
#Finals      
    convert -resize $icon_size icon-fig.png graph.png
}
