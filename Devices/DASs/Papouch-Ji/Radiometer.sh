#!/bin/bash

source /dev/shm/golem/Commons.sh
whoami="Devices/DASs/Papouch-Ji/Radiometer"



SUBDIR="DASs"
ThisDev="Papouch-Ji"

source /golem/Dirigent/Devices/DASs/Drivers/PapouchDAS1210/driver.sh

papouch_ip=Papouch-Ji

shot_no=`CurrentShotDataBaseQuerry shot_no`


#data_dir=$SHM0/$SUBDIR/$ThisDev
data_dir=$SHM0/`dirname $whoami`



function WakeOnLan()
{
    sleep 1;echo "*B1OS3H"|telnet 192.168.2.254 10001
}

function SleepOnLan()
{
    sleep 1;echo "*B1OS3L"|telnet 192.168.2.254 10001
}


function DASArming()
{
    sleep 1
    arm_papouch_das $papouch_ip
}


function OpenSession()
{
    PrepareFilesToSHMs $SHMS Devices/`dirname $Drivers`
}

function Arming()
{
    arm_papouch_das $papouch_ip
}


function RawDataAcquiring()
{
WEBPATH="$PWD"
LastChannelToAcq=$1

#LastChannelToAcq=4


    echo "<html><body>" > das.html
    WebRecDas "<h1>The GOLEM tokamak DAS $ThisDev for Shot #$shot_no </h1>"
    WebRecDas "<h2><a href="http://golem.fjfi.cvut.cz/shots/$shot_no/DASs/$ThisDev/">Data dir</a><h2/>"
    WebRecDas "<img src='plot.jpg'/><br></br>"

    LogItColor 4 "$ThisDev: Start of acquiring"
    for i in `seq $LastChannelToAcq` ; do
            echo -ne ACQ: $i: ;
            read_channel_papouch_das $papouch_ip $i $data_dir/ch$i.csv 40e-3; 
    done
    WebRecDas "</body></html>"
    
    echo -n "set terminal jpeg;unset xtics;set size 1,1;set origin 0,0;set multiplot layout $LastChannelToAcq,1 columnsfirst scale 1.1,1;set datafile separator ',';" >/tmp/foo; for i in `seq 1 $LastChannelToAcq`; do echo -n plot \''ch'$i'.csv'\' u 1:2';';done >>/tmp/foo;echo " unset multiplot" >>/tmp/foo; cat /tmp/foo|gnuplot > ScreenShotAll.png
    convert -resize 200x200 ScreenShotAll.png rawdata.jpg

   
    }
 
