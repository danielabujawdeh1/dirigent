#!/bin/bash

source /dev/shm/golem/Commons.sh


whoami="Devices/DASs/Papouch-St/21_RailProbe_JMetal_remote"
ThisDev=`dirname $whoami|xargs basename`
Drivers="DASs/Drivers/PapouchDAS1210/driver"

source Universals.sh

papouch_ip=$ThisDev

if [[ -f $SHMS/Devices/$Drivers.sh ]]; then 
    source $SHMS/Devices/$Drivers.sh
fi



shot_no=`CurrentShotDataBaseQuerry shot_no`


data_dir=$SHM0/`dirname $whoami`


function OpenSession()
{
    PrepareFilesToSHMs $SHMS Devices/`dirname $Drivers`
}

function LOCArming()
{
    arm_papouch_das $papouch_ip
}

# Remote pres golem-old:
# mkdir -p /dev/shm/golem/
# sshfs golem@192.168.2.116:/dev/shm/golem/ /dev/shm/golem/ -o

function Arming()
{
 
    ssh golem@192.168.2.117 "cd /golem/Dirigent/Devices/DASs/Papouch-St/;source 21_RailProbe_JMetal_remote.sh;LOCArming"
    #arm_papouch_das $papouch_ip
}



function LOC_RawDataAcquiring()
{
#echo "$@"
local LastChannelToAcq=$1

    LogIt "$ThisDev: Start of acquiring"

    for i in `seq 1 $LastChannelToAcq` ; do
            echo -ne ACQ:ch$i@$papouch_ip;read_channel_papouch_das $papouch_ip $i $data_dir/ch$i.csv 40e-3; 
    done
    
    #set title '$shot_no';
    BASE1="set datafile separator ',';unset key;set xrange [3000e-6:15000e-6];set style data lines;set format y '%3.1f';set multiplot;set size 1,0.25;set origin 0,0.75;set ylabel 'ch1';plot 'ch1.csv';set origin 0,0.5;unset xtics;set ylabel 'ch2';plot 'ch2.csv';set origin 0,0.25;unset xtics;set ylabel 'ch3';plot 'ch3.csv';set origin 0,0.0;unset xtics;set ylabel 'ch4';plot 'ch4.csv'" 
	echo "set terminal png;$BASE1"|gnuplot >ScreenShotAll.png
	convert -resize 200x200 ScreenShotAll.png rawdata.jpg
    LogIt "$ThisDev: End of acquiring"
}


function RawDataAcquiring()
{
#echo "$@"
local LastChannelToAcq=$1

    LogIt "$ThisDev: Start of acquiring"

    ssh golem@192.168.2.117 "cd /golem/Dirigent/Devices/DASs/Papouch-St/;source 21_RailProbe_JMetal_remote.sh;LOC_RawDataAcquiring $LastChannelToAcq"
    #for i in `seq 1 $LastChannelToAcq` ; do
    #        echo -ne ACQ:ch$i@$papouch_ip;read_channel_papouch_das $papouch_ip $i $data_dir/ch$i.csv 40e-3; 
    #done
    
    #set title '$shot_no';
    BASE1="set datafile separator ',';unset key;set xrange [3000e-6:15000e-6];set style data lines;set format y '%3.1f';set multiplot;set size 1,0.25;set origin 0,0.75;set ylabel 'ch1';plot 'ch1.csv';set origin 0,0.5;unset xtics;set ylabel 'ch2';plot 'ch2.csv';set origin 0,0.25;unset xtics;set ylabel 'ch3';plot 'ch3.csv';set origin 0,0.0;unset xtics;set ylabel 'ch4';plot 'ch4.csv'" 
	echo "set terminal png;$BASE1"|gnuplot >ScreenShotAll.png
	convert -resize 200x200 ScreenShotAll.png rawdata.jpg
    LogIt "$ThisDev: End of acquiring"
}










