#!/bin/bash
SUBDIR=DASs
ThisDev=`basename $PWD`
BASEDIR="../.."
source $BASEDIR/Commons.sh


SHM="/dev/shm/golem"
WEBPATH="$PWD"

#shot_no=`CurrentShotDataBaseQuerry shot_no` #obsolete



data_dir=$SHM0/$SUBDIR/$ThisDev/

# Function check: echo ping | netcat NIStandardDAS 5020

function Arming()
{
    sleep 1
    mkdir -p $data_dir
    echo ahoj|telnet NIstandard 5020 > /dev/null 2>/dev/null &
    echo OK
}

function WakeOnLan()
{
    #Start NIstandard
	echo 'wakeonlan -i 192.168.2.255 00:17:31:5a:01:20 #NIstandard' 
	wakeonlan -i 192.168.2.255 00:17:31:5a:01:20 
    echo OK
}

function SleepOnLan()
{
    ssh golem_daq@NIstandard 'sudo /sbin/shutdown -h now'
    echo OK
}


function DASsOpenSession()
{

    $LogFunctionGoingThrough
    echo OK
}

#Petisonda Petr Macha & Filip Papousek
#diags=('null' '1-Lim' '2-OLim' '3-BPP' '4-LP' 'null' 'null'  'null' 'null'  'null' 'null' 'null' 'null')
#icondiags=('1-Lim' '2-OLim' '3-BPP' '4-LP' )


function PostDischargeFinals()
{

    shot_no=`cat $BASEDIR/shot_no`
    LogItColor 4 "$ThisDev: Start of acquiring"
    scp -o ConnectTimeout=1 golem_daq@NIstandard:/home/golem_daq/NIdata_6133.lvm .; \
	sed 's/,/\./g' NIdata_6133.lvm > Nidatap_6133.lvm; rm -f NIdata_6133.lvm; \
	#cp /home/svoboda/Dirigent/DASs/NIstandard/PlasmaPosition.py .
	#python2 PlasmaPosition.py acquisition

	#shot_no=0;\
    DataFile=/golem/database/operation/shots/$shot_no/DASs/PlasmaPosition/Nidatap_6133.lvm;\
    echo "set terminal jpeg;set output '$ThisDev.jpg';set bmargin 0;set tmargin 0;set lmargin 10;set rmargin 3;unset xtics;unset xlabel;set multiplot layout 8, 1 title 'GOLEM Shot $shot_no';set xrange [*:*];set yrange [*:*];set style data dots;set ylabel 'ch1';unset ytics;plot '$DataFile' u 1:2 t '' w l lc 1 ;set bmargin 0;set tmargin 0;unset title;set ylabel 'ch2';plot '$DataFile'  u 1:3  w l lc 2 title '';set ylabel 'ch3';plot '$DataFile'  u 1:4  w l lc 3 title '';;set ylabel 'ch4';plot '$DataFile'  u 1:5  w l lc 4 title '';set ylabel 'ch5';plot '$DataFile'  u 1:6 w l lc 5 title '';set ylabel 'ch6';plot '$DataFile'   u 1:7 w l lc 6 title '';set xtics;set xlabel 't [us]';set ylabel 'ch7';plot '$DataFile'  u 1:8 w l lc 7 title ''" | gnuplot
    convert -resize $icon_size $ThisDev.jpg graph.png
    convert /$SHM/Management/imgs/logos/NIOfficialLogo.gif -gravity center graph.png -gravity center +append icon_.png
    convert -bordercolor Black -border 2x2 icon_.png icon.png
	
    echo "<html><body>" > das.html
        WebRecDas "<h1>The GOLEM tokamak $ThisDev for Shot #$shot_no </h1>"
    WebRecDas "<h2><a href="http://golem.fjfi.cvut.cz/shots/$shot_no/DASs/$ThisDev/">Data dir</a><h2/>"
    WebRecDas "<img src='graph1.png'/><br></br>"

    WebRecDas "</body></html>"
    LogIt "$ThisDev: End of acquiring"
    echo OK
}






function ShutDown()
{
	ssh golem_daq@NIstandard 'sudo /sbin/shutdown -h now'

}


function PostDisch()
{
    $LogFunctionGoingThrough
    echo OK
}

