#!/bin/bash

source /dev/shm/golem/Commons.sh

ThisDev="Papouch-Za"
whoami="Devices/DASs/$ThisDev/DoubleRakeProbe_Basic"
Drivers="DASs/Drivers/PapouchDAS1210/driver"

source Universals.sh

papouch_ip=$ThisDev

if [[ -f $SHMS/Devices/$Drivers.sh ]]; then 
    source $SHMS/Devices/$Drivers.sh
fi



shot_no=`CurrentShotDataBaseQuerry shot_no`


data_dir=$SHM0/`dirname $whoami`


function OpenSession()
{
    PrepareFilesToSHMs $SHMS Devices/`dirname $Drivers`
}

function Arming()
{
    arm_papouch_das $papouch_ip
}

# Remote pres golem-old:
# mkdir -p /dev/shm/golem/
# sshfs golem@192.168.2.116:/dev/shm/golem/ /dev/shm/golem/ -o

function REM_Arming()
{
 
    ssh golem@192.168.2.117 "cd /golem/Dirigent/Devices/DASs/Papouch-Za/;source 21_DRP_KHetal_remote.sh;LOC_Arming"
    #arm_papouch_das $papouch_ip
}


function RawDataAcquiring()
{
#echo "$@"
local LastChannelToAcq=$1

    LogIt "$ThisDev: Start of acquiring"
    for i in `seq 1 $LastChannelToAcq` ; do
            echo -ne ACQ:ch$i@$papouch_ip;read_channel_papouch_das $papouch_ip $i $data_dir/ch$i.csv 40e-3; 
    done
    PlotIt
    LogIt "$ThisDev: End of acquiring"
}


function REM_RawDataAcquiring()
{
#echo "$@"
local LastChannelToAcq=$1

    LogIt "$ThisDev: Start of acquiring"

    ssh golem@192.168.2.117 "cd /golem/Dirigent/Devices/DASs/Papouch-Za/;source 21_DRP_KHetal.sh;LOC_RawDataAcquiring $LastChannelToAcq"
    
        #set title '$shot_no';
    #BASE1="set datafile separator ',';unset key;set xrange [3000e-6:20000e-6];set style data lines;set format y '%3.1f';set multiplot;set size 1,0.25;set origin 0,0.75;set ylabel 'ch1';plot 'ch1.csv';set origin 0,0.5;unset xtics;set ylabel 'ch2';plot 'ch2.csv';set origin 0,0.25;unset xtics;set ylabel 'ch3';plot 'ch3.csv';set origin 0,0.0;unset xtics;set ylabel 'ch4';plot 'ch4.csv'" 
	#echo "set terminal png;$BASE1"|gnuplot >ScreenShotAll.png

	PlotIt
}	

function PlotIt()
{
	   rm tmp
    echo  "set datafile separator ',';set terminal png size 1600, 1000;set output 'ScreenShotAll.png';set bmargin 0;set tmargin 0;set lmargin 10;set rmargin 3;unset xtics;unset xlabel;set multiplot layout 6,2 columnsfirst title 'GOLEM Shot $shot_no';set xrange [*:*];set yrange [*:*];set style data dots;unset ytics;"> tmp; for i in `seq 1 6`; do echo "set ylabel 'R$((i-0))';plot 'ch$i.csv' u 1:2 t '' w l lc 1;" >> tmp;done;for i in `seq 7 12`; do echo "set ylabel 'L$((i-6))';plot 'ch$i.csv' u 1:2 t '' w l lc 1;" >> tmp;done
    cat tmp|gnuplot 1>/dev/null 2>/dev/null
	convert -resize 200x200 ScreenShotAll.png rawdata.jpg

}
