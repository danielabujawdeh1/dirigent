import sys
from time import sleep
import numpy as np
from pydcpf.appliances.DAS1210 import Device


def set_ready(ipaddr):
    dev = Device(ipaddr)
    dev.set_ready()
    sleep(0.5)                  # to let 

def get_data_calibrated(ipaddr, channel, output_file, time_length):
    dev = Device(ipaddr)
    t_end, data = dev.get_data_calibrated(int(channel), float(time_length))
    time = np.linspace(0, t_end, data.shape[0])
    np.savetxt(output_file, np.column_stack([time, data]), delimiter=',')

functions = ['set_ready', 'get_data_calibrated']

if __name__ == '__main__':
    try:
        command = sys.argv[1]
    except IndexError:
        print("provide function name as first argument, possible:", functions)
    func = globals()[command]
    func(*sys.argv[2:])
