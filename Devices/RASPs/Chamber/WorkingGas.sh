#!/bin/bash

# To be executed at RASPs

#source WorkingGas.sh;GetReadyTheDischarge;SecurePostDischargeState

source Commons.sh


whoami="Devices/RASPs/Chamber/WorkingGas"
DirToDischargeParams=$SHM0/`dirname $whoami`


source Drivers/GWInstekPSW/driver.sh

QuidoModul='telnet Vacuum_relays_quido 10001'



function RelayON()
{
    echo "*B1OS"$1"H"|$QuidoModul   1>/dev/null 2>/dev/null
}


function RelayOFF()
{
    echo "*B1OS"$1"L"|$QuidoModul  1>/dev/null 2>/dev/null
}

function SetVoltage@GasValveTo()
{
    echo "APPL $1,2;OUTPut:IMMediate ON"|netcat -w 1 GWInstekPSW-a 2268
    echo -ne $1 > $SHML/ActualVoltageAtGasValve
}    

function SetVoltage@GasValveOFF()
{
    echo "OUTPut:IMMediate OFF"|netcat -w 1 GWInstekPSW-a 2268
    echo -ne 0 > $SHML/ActualVoltageAtGasValve
}


function GasH2ON(){ RelayON 12; cp $SHML/WG_calibration_table4H2 $SHML/WG_calibration_table; echo -ne "H2" > $SHML/GasSwitch; }
function GasH2OFF(){ RelayOFF 12; echo -ne "NULL" > $SHML/GasSwitch; }

function GasHeON(){ RelayON 13; cp $SHML/WG_calibration_table4He $SHML/WG_calibration_table; echo -ne "He" > $SHML/GasSwitch; }
function GasHeOFF(){ RelayOFF 13; echo -ne "NULL" > $SHML/GasSwitch; }


function SwitchOutAllGases
{
    SetVoltage@GasValveOFF
    Relax
    GasH2OFF;GasHeOFF
}

function GetReadyTheDischarge()
{ 

    if [ -e $SHMP/pressure ]; 
    then
        p_working_gas_discharge_request=`cat $SHMP/pressure`
    else
        LogIt "No request for Working gas pressure, therefore Discharge with default gas pressure"
        p_working_gas_discharge_request=`cat $SHM/Management/DefaultParameters/pressure`
        cp $SHM/Management/DefaultParameters/pressure $SHMP/pressure
    fi    
    echo $p_working_gas_discharge_request > $DirToDischargeParams/p_working_gas_discharge_request
    LogIt "Going with gas pressure $p_working_gas_discharge_request"
    
    if [ -e $SHMP/gas ]; 
    then
        p_working_gas_type_request=`cat $SHMP/gas`
    else
        LogIt "No request for Working gas type, therefore Discharge with default gas type"
        p_working_gas_type_request=`cat $SHM/Management/DefaultParameters/gas`
        cp $SHM/Management/DefaultParameters/gas $SHMP/gas
    fi    
    echo $p_working_gas_type_request > $DirToDischargeParams/X_working_gas_discharge_request
    LogIt "Going with gas type $p_working_gas_type_request"
        
    cp $SHML/ActualChamberPressuremPa $DirToDischargeParams/p_chamber_pressure_before_discharge
    

    
    if [ $p_working_gas_discharge_request -eq 0 ]; 
    then 
        LogIt "Vacuum discharge !"
    else
        case $p_working_gas_type_request in 
            H)
                LogIt "Discharge with Hydrogen request"
                GasH2ON
                ;;
            He)
                LogIt "Discharge with Helium request"
                GasHeON
                ;;
            "")   
                LogIt "No request for Working gas, therefore Discharge with default H2 gas"
                GasH2ON
        esac
        LogIt "Discharge with $p_working_gas_discharge_request mPa request"
        echo "Discharge Preparation" >>$SHML/PressureLog

        python3 -c "from vacuum.working_gas import fill_chamber; fill_chamber(target_pressure=$p_working_gas_discharge_request,max_time=30)"    
    fi
    
    cp $SHML/ActualChamberPressuremPa $DirToDischargeParams/p_chamber_pressure_predischarge
    DistanceUpdateCurrentShotDataBase "p_chamber_pressure_predischarge=`cat $DirToDischargeParams/p_chamber_pressure_predischarge`"

    
    $LogFunctionPassing;
}

function SecurePostDischargeState()
{
    python3 -c 'from vacuum.working_gas import close_valve; close_valve()'
    SwitchOutAllGases

}


function H2Calibration()
{
    echo "INSERT into chamber_management (event,date,time, shot_no, session_id, chamber_pressure,  forvacuum_pressure, temperature) VALUES ('H2 valve calibration:do','`date '+%y-%m-%d'`','`date '+%H:%M:%S'`', `cat $SHM/shot_no`, `cat $SHM/session_id`,`cat $SHML/ActualChamberPressuremPa`,`cat $SHML/ActualForVacuumPressurePa`, `cat $SHML/ActualChamberTemperature`) "|ssh Dg "cat - |psql -q -U golem golem_database"
    rm -f $SHML/WG_calibration_table4H2
    rm -f $SHML/WG_calibration_table4H2
    SetVoltage@GasValveTo "0";
    sleep 5s
    GasH2ON
    #for i in `seq 15 0.4 29`; do
    #for i in `seq 20 0.25 29`; do
    for i in `seq 20 0.25 35`; do 
        SetVoltage@GasValveTo "$i";
        sleep 4s;
        echo Calibrating at $i V: `cat $SHML/ActualChamberPressuremPa` mPa
        echo $i "  "  `cat $SHML/ActualChamberPressuremPa`  >> $SHML/WG_calibration_table4H2
    done    
    cp $SHML/WG_calibration_table4H2 /tmp/WG_calibration_table4H2_`date "+%y%m%d%H:%M:%S"`
    SetVoltage@GasValveOFF
    Relax
    GasH2OFF

}    


function HeCalibration()
{
    echo "INSERT into chamber_management (event,date,time, shot_no, session_id, chamber_pressure,  forvacuum_pressure, temperature) VALUES ('He valve calibration:do','`date '+%y-%m-%d'`','`date '+%H:%M:%S'`', `cat $SHM/shot_no`, `cat $SHM/session_id`,`cat $SHML/ActualChamberPressuremPa`,`cat $SHML/ActualForVacuumPressurePa`, `cat $SHML/ActualChamberTemperature`) "|ssh Dg "cat - |psql -q -U golem golem_database"
    rm -f $SHML/WG_calibration_table4H2
    rm -f $SHML/WG_calibration_table4He
    SetVoltage@GasValveTo "0";
    sleep 5s
    GasHeON
    for i in `seq 5 0.4 29`; do 
        SetVoltage@GasValveTo "$i";
        sleep 5s;
        echo Calibrating at $i V: `cat $SHML/ActualChamberPressuremPa` mPa
        echo $i "  "  `cat $SHML/ActualChamberPressuremPa`  >> $SHML/WG_calibration_table4He
    done    
    SetVoltage@GasValveOFF
    Relax
    GasHeOFF

}

function xtermH2Calibration(){
    xterm -fg yellow -bg blue -title "Golem H2 calibration" -hold -e /bin/bash -l -c "H2Calibration"
    }


    function WGtestH2
{
    $LogFunctionStart
    GasH2ON;WGtest;GasH2OFF
    $LogFunctionEnd
}


function WGtestHe
{
    $LogFunctionStart
    GasHeON;WGtest;GasHeOFF
    $LogFunctionEnd
}

#@Dirigent:  Dgrs;./Dirigent.sh --wgtest

function WGtest
{
    $LogFunctionStart
    echo "WGtest start" >>$SHML/PressureLog
    python3 -c 'from vacuum.working_gas import fill_chamber; fill_chamber(target_pressure=20,max_time=15)'
    sleep 5s
    python3 -c 'from vacuum.working_gas import close_valve; close_valve()'
    echo "WGtest end" >>$SHML/PressureLog
    $LogFunctionEnd

}

#@RASP:
#nano vacuum/working_gas.py
#golem@chamber:~ $ source WorkingGas.sh ;H2Calibration 
#GasH2ON;SetVoltage@GasValveTo 20;sleep 5;SetVoltage@GasValveTo 0;sleep 1;GasH2OFF
#GasH2ON;python3 -c "from vacuum.working_gas import fill_chamber; fill_chamber(target_pressure=10,max_time=30)";GasH2OFF
#nano vacuum/working_gas.py 


