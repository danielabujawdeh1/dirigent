#!/bin/bash

# To be executed at RASPs

whoami="Devices/RASPs/Chamber/Vacuum"

source Commons.sh



source Drivers/TPG262GNOME232/driver.sh
source Drivers/PapaGo2TC_ETH/driver.sh


QuidoModul='telnet Vacuum_relays_quido 10001'
    
#Devices="Vacuum_gauge Vacuum_rasp Vacuum_relays_quido"

# From start
#source Vacuum.sh;Vents_ON
#RotPumpON
#TMPs_ON


function PrepareSessionEnv@SHM()
{

    MountCentralSHMEnvironment 
}
	

function OpenSession()
{
    nohup bash -c 'source Vacuum.sh; VacuumLog > /dev/null 2>&1 &' >/dev/null 2>&1 &
    sleep 2
    RemoteUpdateCurrentSessionDataBase "start_chamber_pressure=`cat $SHML/ActualChamberPressurePa`,start_for_vacuum_pressure=`cat $SHML/ActualForVacuumPressurePa`"
    
}	


function RelayON()
{
    echo "*B1OS"$1"H"|$QuidoModul   1>/dev/null 2>/dev/null
}


function RelayOFF()
{
    echo "*B1OS"$1"L"|$QuidoModul  1>/dev/null 2>/dev/null
}


#function RotPumpON(){ RelayON 16; } #Before 062021 reconstruction
#function RotPumpOFF(){ RelayOFF 16; } #Before 062021 reconstruction

function RotPumpON(){ echo "*B1OS2H"|telnet 192.168.2.248 10001 1>&- 2>&-; } 
function RotPumpOFF(){ echo "*B1OS2L"|telnet 192.168.2.248 10001 1>&- 2>&-; } 

function TMP2StandbyON(){ RelayON 3; }
function TMP2StandbyOFF(){ RelayOFF 3; }
function TMP2ON(){ RelayON 4; }
function TMP2OFF(){ RelayOFF 4; }
function Vent2ON(){ RelayON 5; }
function Vent2OFF(){ RelayOFF 5; } #2 Jih
function Vent2sON(){ RelayON 10; }
function Vent2sOFF(){ RelayOFF 10; } #2 Jih

function TMP1StandbyON(){ RelayON 6; }
function TMP1StandbyOFF(){ RelayOFF 6; }
function TMP1ON(){ RelayON 7; }
function TMP1OFF(){ RelayOFF 7; }
function Vent1ON(){ RelayON 8; }
function Vent1OFF(){ RelayOFF 8; } #1:Sever
function Vent1sON(){ RelayON 9; }
function Vent1sOFF(){ RelayOFF 9; } #1:Sever



function RotP_ON(){
    $LogFunctionGoingThrough;
    RotPumpON
}

function RotP_OFF(){
    $LogFunctionGoingThrough;
    RotPumpOFF
}




function ReadChamberTemp()
{
    PapagoReadCh1
}





function PostDischarge()
{
    $LogFunctionGoingThrough
    echo "Discharge end" >>$SHML/PressureLog
    cp $SHML/ActualChamberPressuremPa $SHM0/$SUBDIR/$ThisDev/final_chamber_pressure
    echo 25> $SHM0/$SUBDIR/$ThisDev/final_chamber_temperature
    DistanceUpdateCurrentShotDataBase "p_chamber_pressure_after_discharge=`cat $SHML/ActualChamberPressuremPa`"
}


function PostDischargeFinals()
{

    #cat $SHML/GlobalLogbook|grep VacuumLog|awk '{print $1 " " $5 " " $7 }' >$SHM0/$SUBDIR/$ThisDev/Chamber.dat
    gnuplot  -e "set xdata time;set timefmt '%H:%M:%S';set xtics format '%tH:%tM' time;set xlabel 'Time [h:m]';set ylabel 'chamber pressure p_{ch} [mPa]';set xrange [*:*];set title 'Session chamber logbook';set yrange [*:50];set y2range [0:200];set y2tics 0, 20,200;set y2label'Chamber temperature T_{ch} [^o C]';set ytics nomirror;set terminal jpeg;plot '$SHM/ChamberLog' using 1:2 title 'pressure' with lines axis x1y1,'$SHM/ChamberLog' using 1:3 title 'temperature' with lines axis x1y2" > $SHM0/$SUBDIR/$ThisDev/SessionChamber_p_T_Logbook.jpg

       
    convert -resize $icon_size $SHM0/$SUBDIR/$ThisDev/SessionChamber_p_T_Logbook.jpg $SHM0/$SUBDIR/$ThisDev/graph.png
    convert /$SHM/Management/imgs/Chamber_icon.jpg $SHM0/$SUBDIR/$ThisDev/graph.png +append $SHM0/$SUBDIR/$ThisDev/icon_.png
    convert -bordercolor Black -border 2x2 $SHM0/$SUBDIR/$ThisDev/icon_.png $SHM0/$SUBDIR/$ThisDev/icon.png
    Web

}

function WebRecord()
{
    echo $1 >> $SHM0/$SUBDIR/$ThisDev/$ThisDev.html
}

function Web()
{
    cp $SHM/Management/imgs/Chamber.jpg $SHM0/$SUBDIR/$ThisDev/
    
    
    WebRecord "<html><body><h1>$ThisDev</h1>"
    WebRecord "<img src='Chamber.jpg' width='50%'/><br></br>"
    WebRecord "<a href='SessionChamber_p_T_Logbook.jpg'><img src='SessionChamber_p_T_Logbook.jpg' width='60%'/></a>"
    WebRecord "</body></html>"
    
}    



    

function VacuumLog()
{
    echo -ne NULL > $SHML/GasSwitch;
    echo -ne 0 > $SHML/ActualVoltageAtGasValve;
    rm -f $SHM/ChamberLog 
    rm -f $SHML/GlobalLogbook

    while [ 1 ]; do
        for i in `seq 1 5`; do
            ActualChamberPressurePa=`get_chamber_pressure`
            ActualForVacuumPressurePa=`get_forvacuum_pressure`
            ActualChamberTemperature=`PapagoReadCh1`
            #echo $ActualChamberPressurePa > $SUBDIR/$ThisDev/ActualChamberPressurePa #NEJDE, ODSTRELUJE sshfs
            echo -ne $ActualChamberPressurePa > $SHML/ActualChamberPressurePa
            echo -ne  $ActualForVacuumPressurePa > $SHML/ActualForVacuumPressurePa
            echo -ne  $ActualChamberTemperature > $SHML/ActualChamberTemperature
            echo -ne  `echo $ActualChamberPressurePa*1000|bc|xargs printf '%4.2f'|sed 's/,/\./g'` > $SHML/ActualChamberPressuremPa
            echo `date '+%H:%M:%S'` "Chamber: `echo $ActualChamberPressurePa*1000|bc|xargs printf '%4.2f'` mPa, ForVacuum: $ActualForVacuumPressurePa Pa, Gas: `cat $SHML/GasSwitch`, GasValve: `cat $SHML/ActualVoltageAtGasValve|xargs printf '%4.2f'` V">> $SHML/PressureLog
            echo `date '+%H:%M:%S'` " " `echo $ActualChamberPressurePa*1000|bc|xargs printf '%4.2f'` " " `cat $SHML/ActualChamberTemperature|xargs printf '%4.2f'`  >> $SHM/ChamberLog
            #sleep 1 # neni potreba, je to zatim pomale dost
        done
        LogIt "Chamber: `echo $ActualChamberPressurePa*1000|bc|xargs printf '%4.2f'|sed 's/,/\./g'` mPa,  `cat $SHML/ActualChamberTemperature|xargs printf '%4.2f'` C, ForVacuum: $ActualForVacuumPressurePa Pa, Gas: `cat $SHML/GasSwitch`, GasValve: `cat $SHML/ActualVoltageAtGasValve|xargs printf '%4.2f'` V";
    done
}	


function xtermPumpingON(){
    xterm -fg yellow -bg blue -title "Golem pumping start" -hold -e /bin/bash -l -c "PumpingON"
    }

function PumpingON(){
    if [ -e $SHMS/session_date ]; then
        $LogFunctionStart
        echo "INSERT into chamber_management (event,date,time, shot_no, session_id, chamber_pressure,  forvacuum_pressure, temperature) VALUES ('pumping:start','`date '+%y-%m-%d'`','`date '+%H:%M:%S'`', `cat $SHM/shot_no`, `cat $SHM/session_id`,`cat $SHML/ActualChamberPressuremPa`,`cat $SHML/ActualForVacuumPressurePa`, `cat $SHML/ActualChamberTemperature`) "|ssh Dg "$psql_password;cat - |psql -q -U golem golem_database"
        RotPumpON
        LogIt "sleep 2 s to run TMPs ...";sleep 2
        LogIt "Engaging TMPs .."
        TMPs_ON
        LogIt "sleep 20 s to open all valves ...";sleep 20
        LogIt "Opening valves"
        Vents_ON
        $LogFunctionEnd
    else
        critical_error "Please, first open a session"
    fi
}


function xtermPumpingOFF(){
    xterm -fg yellow -bg blue -title "Golem pumping end" -hold -e /bin/bash -l -c "PumpingOFF"
    }


function PumpingOFF()	
{ 
    $LogFunctionStart
    echo "INSERT into chamber_management (event,date,time, shot_no, session_id, chamber_pressure,  forvacuum_pressure, temperature) VALUES ('pumping:end','`date '+%y-%m-%d'`','`date '+%H:%M:%S'`', `cat $SHM/shot_no`, `cat $SHM/session_id`,`cat $SHML/ActualChamberPressuremPa`,`cat $SHML/ActualForVacuumPressurePa`, `cat $SHML/ActualChamberTemperature`) "|ssh Dg "$psql_password;cat - |psql -q -U golem golem_database"
    Relax
    #echo "UPDATE chamber_pumping SET end_time='`date +%H:%M:%S`',end_pressure=`cat $SHML/ActualChamberPressuremPa`,end_temperature=`cat $SHML/ActualChamberTemperature`  WHERE id IN(SELECT max(id) FROM chamber_pumping)"|ssh Dg "cat - |psql -q -U golem golem_database"
    LogIt "Closing Valves and disengaging TMPs"
    Vents_OFF
    TMPs_OFF
    LogIt "sleep 5 s to stop Rotary pump ...";sleep 5
    LogIt "disengaging rotary pump"
    RotPumpOFF
    $LogFunctionEnd
}


function TMPs_ON()
{
    $LogFunctionPassing
    TMP1ON
    TMP2ON
}

function Vents_ON()
{
    $LogFunctionPassing
    Vent1ON
    Vent2ON
    Vent1sON
    Vent2sON
}

function TMPs_OFF()
{ 
    $LogFunctionPassing
    TMP1OFF
    TMP2OFF
}

function Vents_OFF()
{   
    $LogFunctionPassing
    Vent1OFF
    Vent2OFF
    Vent1sOFF
    Vent2sOFF
}
