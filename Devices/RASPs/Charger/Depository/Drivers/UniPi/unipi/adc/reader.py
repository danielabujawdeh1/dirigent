import smbus
from .MCP342x import MCP342x   # converted to python3-compatibility


class AI(MCP342x):

    def __init__(self, channel=1, scale_factor=5.56, bus=None, **kwargs):
        if bus is None:
            bus = smbus.SMBus(1)
        # channel 0 is AI1
        super(AI, self).__init__(bus, 0x68, device='MCP3422', channel=channel-1,
					     scale_factor=scale_factor)


if __name__ == '__main__':
    import sys
    try:
        channel = int(sys.argv[1])
    except (IndexError, ValueError) as err:
        raise ValueError('provide channel number (1 or 2) as first argument')
    ai = AI(channel)
    print(ai.read())
