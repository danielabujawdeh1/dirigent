import os
import time
import RPi.GPIO as GPIO
import logging


# TODO may be set too often
GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

def gpio_low (pin):
    GPIO.setup(pin,GPIO.OUT)  # TODO necessary each time?
    GPIO.output(pin,GPIO.LOW)


def gpio_high (pin):
    GPIO.setup(pin,GPIO.OUT)
    GPIO.output(pin,GPIO.HIGH)


def read_raw(input_n):
    with open('/sys/devices/platform/soc/3f804000.i2c/i2c-1/1-0048/in{}_input'.format(input_n), 'r') as reg:
        value = reg.read()
    return int(value)


def Et_capacitor(return_raw=False):
    p_raw = read_raw(5)
    p = 10**(p_raw/1000.0*(9.98+46.9)/9.98)
    if return_raw:
        return p_raw, p
    else:
        return p


logger = logging.getLogger('vacuum')

def write2log(value):
    logger.debug('[{}] {}'.format(time.strftime('%H:%M:%S'), value))
