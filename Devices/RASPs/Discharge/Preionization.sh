#!/bin/bash

# To be executed at RASPs

source Commons.sh

whoami="Devices/RASPs/Discharge/Preionization"
DirToDischargeParams=$SHM0/`dirname $whoami`

Drivers="Devices/uControllers/Drivers/Arduino8relayModul"
source Drivers/Arduino8relayModul/driver.sh

function PreionHeaterON(){ RelayON 7; }
function PreionHeaterOFF(){ RelayOFF 7; }
function PreionAccelerVoltON(){ RelayON 5; }
function PreionAccelerVoltOFF(){ RelayOFF 5; }
function PreionAMeterProtectON(){ RelayON 6; }
function PreionAMeterProtectOFF(){ RelayOFF 6; }

function DefineTable()
{
    CreateTable discharge.preionization
}


function PrepareSessionEnv@SHM()
{

    MountCentralSHMEnvironment 

}

function GetReadyTheDischarge()
{
# Backward compatibility ..
    LogTheDeviceAction 
    ssh gm 'cd /golem/Dirigent;source Commons.sh;GeneralTableUpdateAtDischargeBeginning "discharge.preionization"'

    echo 1 > $DirToDischargeParams/PreionizationRequest
    # Engage preionization
    #python2 $BASEDIR/Drivers/AC250Kxxx/set_voltage.py `cat $SHMP/PreionizationPowSup4HeaterVoltage` 1>/dev/null 2>/dev/null
    #python2 $BASEDIR/Drivers/AC250Kxxx/set_voltage.py 80 # PreionizationPS
    PreionHeaterON #!!!!! back to ON
    PreionAccelerVoltON
    sleep 3
    PreionAMeterProtectON
}

function SecurePostDischargeState()
{
    # DisEngage preionization
    PreionHeaterOFF
    PreionAccelerVoltOFF
    PreionAMeterProtectOFF
}
