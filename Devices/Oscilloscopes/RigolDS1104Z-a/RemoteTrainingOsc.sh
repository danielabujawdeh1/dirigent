#!/bin/bash

BASEDIR="/golem/Dirigent/"
source $BASEDIR/Commons.sh


ThisDev=RigolDS1104Z-a
COMMAND="netcat -w 1 $ThisDev 5555"
scope_address="nc -w 1 $ThisDev 5555"

whoami="Devices/Oscilloscopes/$ThisDev/RemoteTrainingOsc"


source $BASEDIR/Diagnostics/BasicDiagnostics4TrainCourses/commons/RemoteTrainingOsc.sh

diags=("" "U_Loop" "U_BtCoil" "U_RogCoil" "U_photod")


# e.g. echo ":SINGLe"|nc -w 1 147.32.4.87 5555


function OpenSession()
{
    CommonOpenSession
    
}


function Arming()
{
	echo ":SINGLe"|$scope_address
}



function RawDataAcquiring()
{
    GetOscScreenShot 
    convert -resize $icon_size ScreenShotAll.png rawdata.jpg
    getdata
 
}

function GetOscScreenShot()
{
	echo ":SYSTem:KEY:PRESs MOFF"|$COMMAND
	mRelax
    #echo ":DISPLAY:DATA?  ON,OFF,PNG"|$COMMAND|tail -c +12 > $SHM0/$SUBDIR/$ThisDev/ScreenShot.png
    echo ":DISPLAY:SNAP? png"|netcat -w 4 $ThisDev 5555|tail -c +12 > ScreenShotAll.png
    #echo ":DISPLAY:DATA?  ON,OFF,PNG"|$COMMAND
}

function getdata()
{
    CommonGetData

}

