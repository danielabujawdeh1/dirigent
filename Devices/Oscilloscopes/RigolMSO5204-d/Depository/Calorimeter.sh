#!/bin/bash

BASEDIR="../../.."
source $BASEDIR/Commons.sh


ThisDev=RigolMSO5204-d
COMMAND="netcat -w 1 $ThisDev 5555"
scope_address="nc -w 1 $ThisDev 5555"

whoami="Devices/Oscilloscopes/RigolMSO5204-d/Calorimeter"

diags=("" "RTD_front_top" "RTD_back" "RTD_front_bottom" "Trigger")


# e.g. echo ":SINGLe"|nc -w 1 147.32.4.87 5555


function WakeOnLan()
{
    echo "*B1OS8H"|telnet 192.168.2.254 10001 1>&- 2>&-; 
}

function SleepOnLan()
{
    echo "*B1OS8L"|telnet 192.168.2.254 10001 1>&- 2>&-; 
}



function OpenSession()
{
   echo "
CHANnel1:DISPlay ON;CHANnel1:PROBe 1;CHANnel1:SCALe 5e-2;CHANnel1:OFFSet -20e-3;
CHANnel2:DISPlay ON;CHANnel2:PROBe 1;CHANnel2:SCALe 1e-2;CHANnel2:OFFSet -1;
CHANnel3:DISPlay ON;CHANnel3:PROBe 1;CHANnel3:SCALe 1e-2;CHANnel3:OFFSet -7e-1;
CHANnel4:DISPlay ON;CHANnel4:PROBe 1;CHANnel4:SCALe 1;CHANnel4:OFFSet -3;
TIMebase:DELay:ENABle OFF;TIMebase:MAIN:SCALe 5;TIMebase:MAIN:OFFSet 20;
:STOP;:CLEAR;
:SYSTem:KEY:PRESs MOFF
:TRIGger:EDGE:SOURce CHANnel4;TRIGger:EDGE:SLOPe POS;TRIGger:EDGE:LEVel 3;
"|$scope_address
}



function Arming()
{
	echo ":SINGLe"|$scope_address
}



function RawDataAcquiring()
{
local LastChannelToAcq=4

    getdata $LastChannelToAcq
    GetOscScreenShot
 
}

function GetOscScreenShot()
{
	echo ":SYSTem:KEY:PRESs MOFF"|$scope_address
	mRelax
    echo ":DISPLAY:DATA?  ON,OFF,BMP"|$COMMAND|tail -c +12 > ScreenShotAll.bmp
    convert ScreenShotAll.bmp ScreenShotAll.png
    convert -resize 200x200 ScreenShotAll.png rawdata.jpg
    rm ScreenShotAll.bmp 
}


function getdata()
{
local LastChannelToAcq=$1
#SETUP
    mkdir -p ScopeSetup;quer='XINC XOR XREF START STOP'; echo `for i in $quer; do echo :WAV:$i?";";done`'*IDN?'|$scope_address|sed 's/;/\n/g'|sed \$d > ScopeSetup/answers;for i in $quer; do echo _WAV_$i;done > ScopeSetup/querries;paste ScopeSetup/querries ScopeSetup/answers > ScopeSetup/ScopeSetup; 
    #cat ScopeSetup/ScopeSetup;
    awk '{print "echo " $2 " >ScopeSetup/"$1}' ScopeSetup/ScopeSetup |bash;

#TIME
    _WAV_XINC=`cat ScopeSetup/_WAV_XINC`;_WAV_START=`cat ScopeSetup/_WAV_START`;_WAV_STOP=`cat ScopeSetup/_WAV_STOP`; perl -e '$Time=0;for ( $i='$_WAV_START' ; $i <= '$_WAV_STOP'; $i++ ){printf "%.5e\n", $Time;$Time=$Time+'$_WAV_XINC'}' > Time
    
#DATA
    for CHANNEL in `seq 1 4`; do 
	echo "$ThisDev oscilloscope:  Downloading data for channel $CHANNEL"
	echo ":WAV:SOURCE CHAN$CHANNEL;:WAV:FORM ASCii;:WAV:MODE NORM" |$scope_address;sleep 0.5s;echo ":WAV:DATA?" |$scope_address|cut -c 12-|sed 's/,/\n/g'>data
	paste -d, Time data > ${diags[$CHANNEL]}.csv
	gnuplot -e 'set datafile separator ",";set terminal jpeg;plot "'${diags[$CHANNEL]}.csv'" w l t "'${diags[$CHANNEL]/_/\\\\_}'"' > ${diags[$CHANNEL]}.jpg;
    done
    rm Time data

}
