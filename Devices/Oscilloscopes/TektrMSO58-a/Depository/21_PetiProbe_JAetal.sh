#!/bin/bash

source /dev/shm/golem/Commons.sh
whoami="Devices/Oscilloscopes/TektrMSO58-a/21_PetiProbe_JAetal"

source Universals.sh


#Channels:
Nodiags=3
trigger=NULL
Nomaths=1



function OpenSession()
{
    echo "
    :ACQUIRE:MODE HIRes;
    :HORIZONTAL:MODE:SAMPLERATE 1e6;
    :SAVEon:TRIG ON;
    :SAVEON:WAVEform ON;
    :SAVEON:WAVEFORM:FILEFORMAT SPREADSheet;
    :SAVEon:FILE:DEST 'L:/';
    :SAVEON:IMAGE ON;
    :SAVEON:FILE:NAME 'TektrMSO58';
    :SAVEON:WAVEform:SOURCE ALL"|$COMMAND 1>/dev/null 2>/dev/null
    
    # for i in `seq 1 8`;do
    #    echo ":CH$i:SCALE 5;:CH$i:OFFSET 0;CH$i:LABEL:NAME 'ch$i';:DISplay:GLObal:CH$i:STATE ON"|$COMMAND  1>/dev/null 2>/dev/null
    #done

    echo "
    :CH1:SCALE 4;:CH1:OFFSET 0;:DISplay:GLObal:CH1:STATE ON;
    :CH2:SCALE 10;:CH2:OFFSET 0;:DISplay:GLObal:CH2:STATE ON;
    :CH3:SCALE 1;:CH3:OFFSET 0;:DISplay:GLObal:CH3:STATE ON;
    :CH4:SCALE 10;:CH2:OFFSET 0;:DISplay:GLObal:CH4:STATE ON;
    :CH5:SCALE 10;:CH3:OFFSET 0;:DISplay:GLObal:CH5:STATE ON;
    :CH8:SCALE 2;:CH8:OFFSET 0;:DISplay:GLObal:CH8:STATE ON;
    :CH1:LABel:NAME 'U_loop-b';
    :CH2:LABel:NAME 'U_bias';
    :CH3:LABel:NAME 'U_current';
    :CH4:LABel:NAME 'U_fl_LP';
    :CH5:LABel:NAME 'U_fl_BPP';
    :CH8:LABel:NAME 'Trigger'"|$COMMAND 1>/dev/null 2>/dev/null
    
    
    
    ExternDataAvailabilityTest
    
    PrepareFilesToSHMs $SHMS Devices/`dirname $Drivers`

    
}


   
function Arming()
{

    LogTheDeviceAction

    rm -f /home/golem/tektronix_drop/TektrMSO58*.csv
    rm -f /home/golem/tektronix_drop/TektrMSO58*.png
    local u_fg=`cat $SHM0/Diagnostics/PetiProbe/Parameters/u_fg`;
      
    echo "
    :SAVEon:TRIG ON;
    :SAVEON:WAVEform ON;
    :SAVEON:WAVEFORM:FILEFORMAT SPREADSheet;
    :SAVEon:FILE:DEST 'L:/';
    :SAVEON:IMAGE ON;
    :SAVEON:FILE:NAME 'TektrMSO58';
    :CH2:SCALE `echo 'scale=2;('$u_fg'*20)/5'|bc`;:CH2:OFFSET 0;
    :DISplay:GLObal:CH8:STATE ON;
    :SAVEON:WAVEform:SOURCE ALL"|$COMMAND 1>/dev/null 2>/dev/null
    
    #echo ":CH3:SCALE `echo 'scale=2;('$u_fg'*20)/5*47/1000'|bc`;:CH2:OFFSET 0;"|$COMMAND 1>/dev/null 2>/dev/null
    
	SingleSeq
}


function Web() 
{
    echo "<html><body>" > das.html
    WebRecDas "<h1>$ThisDev@GOLEM for Shot #$shot_no </h1>"
    WebRecDas "<a href="http://golem.fjfi.cvut.cz/shots/$shot_no/DASs/$ThisDev/">Data dir</a><br/>"
    WebRecDas "<a href="ScreenShotAll.png"><img src='ScreenShotAll.png' width='50%'></a><br/>"

 
}
   
   
   
   




function RawDataAcquiring()
{
    # for i in `seq 1 5`; do echo Call from Petiprobe $i;sleep 1; done
    getdata "1 2 3 4 5"

    echo "
    :DISplay:GLObal:CH6:STATE OFF;
    :DISplay:GLObal:CH7:STATE OFF;
    :DISplay:GLObal:CH8:STATE OFF"|$COMMAND  1>/dev/null 2>/dev/null


    #GetOscScreenShot
}
    
