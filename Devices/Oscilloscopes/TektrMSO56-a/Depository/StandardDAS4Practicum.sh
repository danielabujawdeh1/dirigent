#!/bin/bash
SUBDIR=DASs
SHM="/dev/shm/golem"
ThisDev=StandardDAS
QUERRY="netcat -w 5 $ThisDev 4000"
COMMAND="netcat -q 1 $ThisDev 4000"
WEBPATH="$PWD"



source $SHM/Commons.sh
source $SHM/Tools.sh


shot_no=`CurrentShotDataBaseQuerry shot_no`
#shot_no=31839 # just for testing




function OpenSession()
{
    echo ":ACQUIRE:MODE HIRes"|$COMMAND
    echo ":HORIZONTAL:MODE:SAMPLERATE 1e6"|$COMMAND
    echo ":SAVEON:WAVEFORM:FILEFORMAT SPREADSheet"|$COMMAND
    echo ":SAVEon:FILE:DEST 'L:/'"|$COMMAND
    echo ":SAVEON:WAVEform ON"|$COMMAND
    echo ":SAVEon:TRIG ON"|$COMMAND
    echo ":SAVEON:IMAGE ON"|$COMMAND
    echo ":SAVEON:FILE:NAME 'TektrMSO56'"|$COMMAND
    echo ':SAVEON:WAVEform:SOURCE ALL'|$COMMAND
    echo "CH1:LABel:NAME 'U_loop,coil'"|$COMMAND
    echo "CH2:LABel:NAME 'U_Bt,coil'"|$COMMAND
    echo "CH3:LABel:NAME 'U_Rog,coil'"|$COMMAND
    echo "CH4:LABel:NAME 'U_Photod,coil'"|$COMMAND
    echo "CH5:LABel:NAME 'Null'"|$COMMAND
    echo "CH6:LABel:NAME 'Trigger'"|$COMMAND
    # -INTG(Ch3)*5.3e6

    echo OK
}


function CommonInitDischarge()
{
    echo OK
}

function CloseSession()
{
    echo OK
}

   #diags=('null' 'loop_voltage' 'toroidal_field_coil_voltage' 'rogowski_coil_voltage' 'photodiode_alpha' )
   
   

function SingleSeq()
{
    echo "FPANEL:PRESS SINGLESEQ"|$COMMAND
}   


function ForceTrig()
{
    echo "FPANEL:PRESS FORCetrig"|$COMMAND
}

function Arming()
{
    sleep 1
    OpenSession # Workarround, zlobi to ..
    rm -f /home/golem/tektronix_drop/*.csv
    rm -f /home/golem/tektronix_drop/*.png
    #mkdir -p $SHM0/$SUBDIR/$ThisDev/
	SingleSeq
    echo ":DISplay:GLObal:CH2:STATE ON"|$COMMAND
    echo ":DISplay:GLObal:CH3:STATE ON"|$COMMAND
    echo ":DISplay:GLObal:CH6:STATE ON"|$COMMAND

    echo OK
}


Nodiags=5
trigger=6
Nomaths=2
diags=('null' 'LoopVoltageCoil' 'BtCoil' 'RogowskiCoil' 'LeyboldPhotodiodeNoFilter' 'InnerQuadrupole' 'Trigger')
#diags=('null' 'LoopVoltageCoil' 'BtCoil' 'RogowskiCoil' 'BPP' 'Tolim' 'Trigger')
maths=('null' 'BtCoil' 'RogowskiCoil')


function Web() 
{
    cp $dirigent_dir/DASs/TektrMSO56.a/das.ipynb . # will be better
    sed -i "s/shot_no\ =\ 0/shot_no\ =\ `cat /dev/shm/golem/shot_no`/g" das.ipynb
    # jupyter-nbconvert index.ipynb # Zatim tady nejde ...
    convert -resize $icon_size ScreenShot.png icon.png 

}

function WebStandard() 
{
    echo "<html><body>" > das.html
    WebRecDas "<h1>$ThisDev@GOLEM for Shot #$shot_no </h1>"
    WebRecDas "<a href="http://golem.fjfi.cvut.cz/shots/$shot_no/DASs/$ThisDev/">Data dir</a><br/>"
    WebRecDas "<a href="ScreenShotAll.png"><img src='ScreenShotAll.png' width='50%'></a><br/>"
    for i in `seq 1 6`;do
        diag_id=${diags[$i]}
        echo "set terminal jpeg;set datafile separator ',';set title '$shot_no';set style data dots;set ylabel '$diag_id  [V]'; set xlabel 'Time [s]';set output '$diag_id.jpg';plot '"$diag_id"_raw.csv' u 1:2 w l title ''"|gnuplot 
        WebRecDas "<h2>Standard diagnostics:$diag_id (raw voltage signal)</h2>"
        WebRecDas "<img src='$diag_id.jpg'/></br>"
        WebRecDas "<a href='http://golem.fjfi.cvut.cz/shots/$shot_no/DASs/$ThisDev/"$diag_id"_raw.csv'/>Data link</a></br>"
        #WebRecDas "<a href='"$diag_id"_raw.csv'/>Raw data</a></br>"
    done    
       for i in `seq 1 2`;do
        diag_id=${maths[$i]}
        echo "set terminal jpeg;set datafile separator ',';set title '$shot_no';set style data dots;set ylabel '$diag_id  [V]'; set xlabel 'Time [s]';set output '"$diag_id"math.jpg';plot '"$diag_id"_integrated.csv' u 1:2 w l title ''"|gnuplot 
        WebRecDas "<h2>Standard diagnostics:$diag_id (recalculated signal)</h2>"
        WebRecDas "<img src='"$diag_id"math.jpg'/></br>"
        WebRecDas "<a href='http://golem.fjfi.cvut.cz/shots/$shot_no/DASs/$ThisDev/"$diag_id"_integrated.csv'/>Data link</a></br>"
        #WebRecDas "<a href='"$diag_id"_integrated.csv'/>Raw data</a></br>"
    done    
#    cd ..

}

function PostDischargeFinals()
{
    $LogFunctionGoingThrough
    getdata
    echo ":DISplay:GLObal:CH2:STATE OFF"|$COMMAND
    echo ":DISplay:GLObal:CH3:STATE OFF"|$COMMAND
    echo ":DISplay:GLObal:CH6:STATE OFF"|$COMMAND
    GetOscScreenShot
    Web 
    echo OK

}
    

function getdata ()
{
    LogIt "$ThisDev: Start of acquiring"
    ls -all /home/golem/tektronix_drop/* > $SHM0/DASs/$ThisDev/ls-all
    
    tail -n +10  `ls -d /home/golem/tektronix_drop/*|grep TektrMSO56_ch1` > $SHM0/DASs/$ThisDev/LoopVoltageCoil_raw.csv
    tail -n +10  `ls -d /home/golem/tektronix_drop/*|grep TektrMSO56_ch2` > $SHM0/DASs/$ThisDev/BtCoil_raw.csv
    tail -n +10  `ls -d /home/golem/tektronix_drop/*|grep TektrMSO56_ch3` > $SHM0/DASs/$ThisDev/RogowskiCoil_raw.csv
    tail -n +10  `ls -d /home/golem/tektronix_drop/*|grep TektrMSO56_ch4` > $SHM0/DASs/$ThisDev/LeyboldPhotodiodeNoFilter_raw.csv
    tail -n +10  `ls -d /home/golem/tektronix_drop/*|grep TektrMSO56_ch5` > $SHM0/DASs/$ThisDev/NULL_raw.csv
    tail -n +10  `ls -d /home/golem/tektronix_drop/*|grep TektrMSO56_ch6` > $SHM0/DASs/$ThisDev/Trigger_raw.csv
    tail -n +10  `ls -d /home/golem/tektronix_drop/*|grep TektrMSO56_math1` > $SHM0/DASs/$ThisDev/BtCoil_integrated.csv
    tail -n +10  `ls -d /home/golem/tektronix_drop/*|grep TektrMSO56_math2` > $SHM0/DASs/$ThisDev/RogowskiCoil_integrated.csv
    mv  `ls -d /home/golem/tektronix_drop/*|grep png|grep TektrMSO56` $SHM0/DASs/$ThisDev/ScreenShotAll.png
    
    LogIt "$ThisDev: End of acquiring"
    
  
}	

function GetOscScreenShot()
{
    #null: /usr/local/lib/python2.7/dist-packages/pkg_resources/py2_warn.py:22: UserWarning: Setuptools will stop working on Python 2
    /usr/bin/python $SHM/Drivers/TektrMSO5/main.py save_screenshot $PWD/ScreenShot.png 2>/dev/null
    echo OK
}


function PostDisch()
{
    $LogFunctionGoingThrough
    echo OK
}

function sandbox ()
{
    echo ":data:source ch1"|$COMMAND
    XINCR=`echo ":WFMPRE:XINCR?"|$QUERRY`;echo $XINCR > XINCR.osc

}
	

#./TektrMSO56Standard.sh -r PostDischargeFinals
TASK=$1
COMMANDLINE=`echo $@|sed 's/-r //g'`

case "$TASK" in
   "")
      RETVAL=1
      ;;
      --raw_command|-r)
      RsyncDeviceFromDirigent
      $COMMANDLINE
      ;;
      --rsync)
      RsyncDeviceFromDirigent
      ;;
esac


