#!/bin/bash

BASEDIR="../../.."
source $BASEDIR/Commons.sh

whoami="Devices/PowerSupplies/NIMcrate/HXRdiagnostics"


CommDevice=/dev/ttyUSB0

function WakeOnLan()
{
    echo waking NIM crate:
    shuf -i 0-5|head -1|xargs sleep # To Protect electrical grid
    echo "*B1OS8H"|telnet 192.168.2.254 10001 1>&- 2>&-; #F30 kamery
    echo OK
}

function SleepOnLan()
{
    echo "*B1OS8H"|telnet 192.168.2.254 10001 1>&- 2>&-; #F30 kamery
    echo OK
}

function SetupHVs()
{
    for i in $HXR_probes; do 
        Setup=S_$i
        echo Processing $i ...
        eval "SetupHV@channel \${$Setup[7]} \${$Setup[6 ]}"

    done
}

function ZeroHVs()
{
    for i in `seq 0 3`; do 
        SetupHV@channel 0 $i
    done
}



function DatabaseEntry()
{
    for i in $HXR_probes; do 
        Setup=S_$i
        echo Processing $i ...
            eval coor_x=\${$Setup[0]}
            eval coor_y=\${$Setup[1]}
            eval coor_z=\${$Setup[2]}
            eval coor_theta=\${$Setup[3]}
            eval coor_phi=\${$Setup[4]}
            eval voltage=\${$Setup[7]}

            echo "UPDATE shots SET \
            x_"$i"=$coor_x,\
            y_"$i"=$coor_y,\
            z_"$i"=$coor_z,\
            theta_"$i"=$coor_theta,\
            phi_"$i"=$coor_phi,\
            voltage_"$i"=$voltage\
            WHERE shot_no IN(SELECT max(shot_no) FROM shots)"|psql -q -U golem golem_database 
    done
}


function OpenSession()
{
            echo OK
}


function SetupHV@channel # 1:what [V] at 2:channel [#]
{
    if [ $1 -lt 0 ]; then 
        echo negative request $1 @ ch$2; 
        printf "%s\r\n" ":OUTP:POL n,(@$2)\r\n" > $CommDevice
    else 
        echo positive request at ch$2;
    fi
    printf "%s\r\n" ":VOLT  $1,(@$2)\r\n" > $CommDevice
    printf "%s\r\n" ":VOLT ON,(@$2)\r\n" >$CommDevice
}


#for i in `seq 0 3`;do source HXRdiagnostics.sh;SetupHV@channel 600 $i;done

# 490  compgen -g
# 491  sudo usermod -a -G tty svoboda
# 498  sudo usermod -a -G dialout svoboda
# 495  cd /golem/Dirigent/Devices/PowerSupplies/NIMcrate/
# 499  exit (logout a login)
# 500  cd /golem/Dirigent/Devices/PowerSupplies/NIMcrate/
# 501  source HXRdiagnostics.sh;SetupHV@channel 400 1


