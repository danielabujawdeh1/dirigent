#!/bin/bash
BASEDIR="../../../"
source $BASEDIR/Commons.sh


whoami="Devices/PowerSupplies/GWInstekPSW-b/BiasingElectrode"

# echo ""|netcat -w 1 ChargerOsc  5555



TheDevice=GWinstekPSW-b


# SendCommandToTheDevice "*idn?"

function SendCommandToTheDevice()
{
    echo $1|netcat -w 1 $TheDevice 2268
}




function Arming() 
{ 
    SendCommandToTheDevice "APPL `cat $SHM0/$whoami/Parameters/u_biaselectrode`,2"
    #SendCommandToTheDevice "APPL 20,2"
    SendCommandToTheDevice "OUTPut:IMMediate ON"
}

function SecurePostDischargeState()
{
    $LogFunctionGoingThrough
    SendCommandToTheDevice "OUTPut:IMMediate OFF"
}

