#!/bin/bash


source /dev/shm/golem/Commons.sh


whoami="Devices/PowerSupplies/RigolPS831A-a/HVsubstitute"

ThisDev=`dirname $whoami|xargs basename`

COMMAND="netcat -w 1 $ThisDev 5555"



function GetReadyTheDischarge()
{
    #echo ":INST CH2;:CURR 2;:VOLT 30;:OUTP CH2,ON"|netcat -w 1 RigolPS831Aa 5555
    echo ":INST CH3;:CURR 0.1;:VOLT -30;:OUTP CH3,ON"|netcat -w 1 HVsubstitute 5555
}

function SecurePostDischargeState()
{
   #echo ":OUTP CH2,OFF"|netcat -w 1 RigolPS831Aa 5555 # Tests on RigolPS
    echo ":OUTP CH3,OFF"|netcat -w 1 HVsubstitute 5555 # Tests on RigolPS
} 

#DEPOT

#echo ":INST CH1;:CURR 2;:VOLT 5;:OUTP CH1,ON"|netcat -w 1 192.168.2.60 5555

