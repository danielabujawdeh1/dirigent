#!/bin/bash

basedir="../../.."
source $basedir/Commons.sh

diag_id=`basename $PWD`
setup_id=Basic
whoami="Devices/Tools/$diag_id/$setup_id"

DAS="Oscilloscopes/RigolMSO5204-d/BiasingElectrode"

Devices="PowerSupplies/Kepco100-4D-a/BiasingElectrode FunctionGenerators/RigolDG1032Z-a/BiasingElectrode $DAS"
#RelayBoards/Quido8-a/TokamakSouth 

#PowerSupplies/GWInstekPSW-b/BiasingElectrode 

function PingCheck ()
{
  :
  #PingCheckNotEthDevice $whoami
}


function WakeOnLan(){ 
    echo "*B1OS6H"|telnet 192.168.2.254 10001 1>&- 2>&-; # Zasuvky SEB

 } 

function SleepOnLan(){ 
    echo "*B1OS6L"|telnet 192.168.2.254 10001 1>&- 2>&-; # Zasuvky SEB
}

function DefineDiagTable()
{
    CreateDiagnosticsTable $diag_id
    
}

function GetReadyTheDischarge ()
{
  GeneralDiagnosticsTableUpdateAtDischargeBeginning $diag_id #@Commons.sh
#  CallFunction Devices/RelayBoards/Quido8-a/TokamakSouth CurrentClampON 
}


LastChannelToAcq=4


function PostDischargeAnalysis
{
#    CallFunction Devices/RelayBoards/Quido8-a/TokamakSouth CurrentClampOFF
    GeneralDAScommunication $DAS RawDataAcquiring $LastChannelToAcq
    ln -s $basedir/Devices/`dirname $DAS/` DAS_raw_data_dir

    cp DAS_raw_data_dir/*.csv .
    
    issue="I_PotentDiff"
    paste -d, U_R-BiasEl.csv U_R-PowSup.csv |awk -F "," '{print $1","($4-$2)/20}' > $issue.csv
    gnuplot -e 'set datafile separator ",";set terminal jpeg;plot "'$issue.csv'" w l t "'${issue/_/\\\\_}'"' > $issue.jpg;

    issue="I_CurrPrb"
    cat U_CurrPrb.csv |awk -F "," '{print $1","$2/(50e-3)}' > $issue.csv
    gnuplot -e 'set datafile separator ",";set terminal jpeg;plot "'$issue.csv'" w l t "'${issue/_/\\\\_}'"' > $issue.jpg;

    gnuplot -e 'set datafile separator ",";set terminal jpeg;plot "I_PotentDiff.csv" w l t "I\\_PotentDiff","I_CurrPrb.csv" w l t "I\\_CurrPrb"' > Result.jpg;
    
    homepage_row
      
}



function homepage_row
{
local cesta=`dirname $whoami`

    echo "<tr>
    <td valign=bottom><a href=$cesta/><img src=$cesta/name.png  width='$namesize'/></a></td>
    <td valign=bottom><a href=$cesta/expsetup.svg><img src=$cesta/setup.png /></a></td>
        
   
    <td valign=bottom><a href=Devices/`dirname $DAS`/ScreenShotAll.png><img src='Devices/`dirname $DAS`/ScreenShotAll.png'  width='$iconsize'/></a></td>
    <td>
    <a href=$gitlabpath/Devices/`dirname $DAS` title="Gitlab4`dirname $DAS`">$gitlabicon</a><br></br>
    <a href=Devices/`dirname $DAS` title="Data directory">$diricon</a><br></br>
    </td>
    
    <td>
    <a href=$cesta/Result.jpg><img src='$cesta/Result.jpg'  width='$iconsize'/></a>
    </td>
    <td>
    <a href=$cesta/Parameters/ title="Parameters">$parametersicon</a><br></br>
    <a href=$gitlabpath/$cesta/ title="Gitlab4$cesta">$gitlabicon</a>
    <a href=$cesta/ title="Directory">$diricon</a>
    <a href=$dbpath/Diagnostics/$diag_id/ title="Database">$sqlicon</a>
    </td>

    
    </tr>" > include.html
}
