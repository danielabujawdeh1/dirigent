/*
  Blink
  Turns on an LED on for one second, then off for one second, repeatedly.
 
  This example code is in the public domain.
 */
 
// Pin 13 has an LED connected on most Arduino boards.
// give it a name:


// the setup routine runs once when you press reset:
void setup() {                
  // initialize the digital pin as an output.
    
  pinMode(2, OUTPUT);                                                                                                                                                                                                                         
  pinMode(3, OUTPUT);                                                                                                                                                                                                                         
  pinMode(4, OUTPUT);                                                                                                                                                                                                                         
  pinMode(5, OUTPUT);                                                                                                                                                                                                                         
  pinMode(6, OUTPUT);                                                                                                                                                                                                                         
  pinMode(7, OUTPUT);                                                                                                                                                                                                                         
  
  
  
// delay(1000);

  // DAS
  digitalWrite(2, HIGH);   
  delay(1);               
  // Bt
  digitalWrite(3, HIGH);   
  delay(1);               
  // Et
  digitalWrite(4, HIGH);   
  // Stabilizace Vnitrni quadrupol
    delay(3);               
  digitalWrite(5, HIGH);
  

  
  delay(3);

  digitalWrite(2, LOW);   
  digitalWrite(3, LOW);   
  digitalWrite(4, LOW);   
  digitalWrite(5, LOW);   
  digitalWrite(6, LOW);   
  digitalWrite(7, LOW);   
   
  
}

// the loop routine runs over and over again forever:
void loop() {
}
