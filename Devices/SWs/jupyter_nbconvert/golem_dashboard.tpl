{%- extends 'full.tpl' -%}


{% block any_cell %}
    {% set delims_row = cell['metadata']['golem'].get('row') %}
    {% if delims_row != 'end' %}
<div class="{{ cell['metadata']['golem'].get('col', 'col-md-12') }}">
    {% endif %}
    {% if delims_row == 'start' %}
    <div class="row">
    {% elif delims_row == None %}
    {{ super() }}
    {% endif %}
    {% if delims_row != 'start' %}
</div>
    {% endif %}
{% if delims_row == 'end' %}
</div>
{% endif %}
{% endblock any_cell %}


{% block body %}
<body>
  <div tabindex="-1" id="notebook" class="border-box-sizing">
    <div class="container-fluid" id="notebook-container">
{{ super.super() }}
    </div>
  </div>
</body>
{%- endblock body %}
