#/bin/sh

#!/bin/bash
source /dev/shm/golem/Commons.sh
whoami="Devices/Converters/Gnome232-a/VacuumGauge"


function comm_exec_tpg262gnome232() {
    python3 -c "from vacuum.tpg262_gnome232 import TPG262GNOME232; ret = TPG262GNOME232('192.168.2.246').$1"
}

function get_tpg_pressure() {
    comm_exec_tpg262gnome232 "get_pressure($1); print(ret)"
}

function get_chamber_pressure() {
    get_tpg_pressure 1
}

function get_forvacuum_pressure() {
    get_tpg_pressure 2
}
