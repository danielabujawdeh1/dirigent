#!/bin/bash
source /dev/shm/golem/Commons.sh

whoami="Devices/FunctionGenerators/RigolDG1032Z-a/FG4ElstatProbes"

ThisDev=RigolDG1032Z-a.golem
#echo "*IDN?"|netcat -w 1 192.168.2.171 5555


COMMAND="netcat -w 1 $ThisDev 5555"



function WakeOnLan()
{
local Adresa="NETIO_230B-a"

	wget --quiet http://$Adresa/tgi/control.tgi?login=p:admin:admin -O /dev/null
	sleep 1s|tr -d '\n'
	wget --quiet http://$Adresa/tgi/control.tgi?port=1111 -O /dev/null 
}

function ShutDown()
{
    SleepOnLan
}    

function SleepOnLan()
{
local Adresa="NETIO_230B-a"

	wget --quiet http://$Adresa/tgi/control.tgi?login=p:admin:admin -O /dev/null
	sleep 1s|tr -d '\n'
	wget --quiet http://$Adresa/tgi/control.tgi?port=0000 -O /dev/null 
}


function Arming()
{
local Probe=RailProbe # lépe!

    LogTheDeviceAction
     #Freq [Hz],Ampl [V],Offset,Phase
    #echo ":SOUR1:APPL:SIN 10000,0.25,0,0;:OUTP1 ON"|$COMMAND

    echo ":SOUR1:APPL:SIN `cat $SHM0/Diagnostics/$Probe/Parameters/f_fg`,`cat $SHM0/Diagnostics/$Probe/Parameters/u_fg`,`cat $SHM0/Diagnostics/$Probe/Parameters/offset_fg`,0;"|$COMMAND
    mRelax
    echo ":OUTP1 ON"|$COMMAND 

}

function SecurePostDischargeState()
{
    echo "OUTP1 OFF"|$COMMAND
} 

