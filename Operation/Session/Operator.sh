#!/bin/bash

source /dev/shm/golem/Commons.sh

whoami="Operation/Session/Operator"
#backword compatibilty:
SUBDIR=`dirname $whoami`
ThisDev=`basename $whoami`



Devices="ITs/NAS/NAS"
#Drivers="remote_web_app"
#Analysis="Homepage"


# Session stuff
# **********************************************************


function PrepareSessionEnv@SHM()
{
   PrepareEnvironment@SHM $SHMS
   cp $SW_dir/Dirigent.sh $SW_dir/Commons.sh $SHMS/
   cp $SHM/* $SHMS/
   mkdir -p $SHML; #Logbooks
   chmod -R g+rwx $SHM
   mkdir -p $SHMS/SessionLogBook
   SubmitTokamakState "idle" 
   rsync  -r $SW_dir/Management/ActualParameters $SW_dir/Management/DefaultParameters $SHM/Management/

}	



function OpenSession(){


    echo -n Session_id: ; echo $((`CurrentShotDataBaseQuerry "shot_no"`+1))|tee $SHM/session_id $SHMS/session_id
    date "$date_format" > $SHMS/session_date
    echo $Mission > $SHMS/session_mission
    
    

    $LogFunctionStart
    LogIt Date: `cat $SHMS/session_date`;
    LogIt "Session setup:"
    LogIt "=================="
    LogIt Session ID: `cat $SHM/session_id`;
#    LogIt Mission: $Mission
#    for i in $Everywhere; do
#        LogIt $i: ${!i}
#    done    
    LogIt "=================="

    zip -qr $SHMS/GolemCntrl * -x '*.git*'

    # pSQL Database 
    InsertCurrentSessionDataBase "(start_shot_no, start_timestamp, session_mission, session_setup, onstage_wave, others_wave, staff_list) VALUES (`cat $SHM/session_id`,'`cat $SHMS/session_date`', '$Mission', '`cat $SHM/session_setup_name`',  '$OnStage_wave', '$Others_wave','$Staff');" 

      
    EchoItColor 1 "Do not forget to make Test dlouhodobe stability (start Radiometr)"
    EchoItColor 1 "Do not forget to open gas valves at every session beginning! (naprogramuj test rozdilu ocekavani proti realite)"
    EchoItColor 1 "When necessary, do WG calibration !!"
    FakeWGcalH2 # Fake calibration ToDo ...

    OpenJson
       
    ssh pi@discharge 'sudo amixer cset numid=1 -- 90' #set volume of the speaker
  
}

function OpenJson()
{
    python3 /golem/Dirigent/Operation/RemoteOperation/remote_web_app/json_status4remote.py &
    disown `echo $!`
}




function CloseSession(){
    UpdateCurrentSessionDataBase "end_shot_no=`cat $SHM/shot_no`, end_timestamp='`date "$date_format"`', end_chamber_pressure=`cat $SHML/ActualChamberPressuremPa`"
    
    rsync -a $SHM/ActualSession/ /golem/database/operation/sessions/`CurrentSessionDataBaseQuerry "start_shot_no"`
    rm nohup.out 
    
    SubmitTokamakState "offline" 
    Relax
    pkill -f json_status4remote.py 
    killall tail >/dev/null
    killall python3 >/dev/null
    rm -rf $SHM; # Final farewell
    

}



