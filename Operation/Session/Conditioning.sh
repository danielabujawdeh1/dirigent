#!/bin/bash


source /dev/shm/golem/Commons.sh



Devices="Chamber_thermistor"

SUBDIR=Operation/Session
ThisDev=Conditioning

function PingCheck() { 
    CommonPingAllDevices  # @Commons.sh
}


function RelayON()
{
    echo "*B1OS"$1"H"|$QuidoModul   1>/dev/null 2>/dev/null
}


function RelayOFF()
{
    echo "*B1OS"$1"L"|$QuidoModul  1>/dev/null 2>/dev/null
}

function BakingON(){ RelayON 1; }
function BakingOFF(){ RelayOFF 1; }
function GlowDischPSON(){ RelayON 11; }
function GlowDischPSOFF(){ RelayOFF 11; }




function GlowDischInitiate()
{
    $LogFunctionPassing;
    GlowDischPSON
    TMP1StandbyON
    TMP2StandbyON
    Vent1OFF
    GasHeON
    echo "INSERT into chamber_management (event,date,time, shot_no, session_id, chamber_pressure,  forvacuum_pressure, temperature) VALUES ('glow discharge:init','`date '+%y-%m-%d'`','`date '+%H:%M:%S'`', `cat $SHM/shot_no`, `cat $SHM/session_id`,`cat $SHML/ActualChamberPressuremPa`,`cat $SHML/ActualForVacuumPressurePa`, `cat $SHML/ActualChamberTemperature`) "|ssh Dg "$psql_password;cat - |psql -q -U golem golem_database"
    #echo "INSERT into 	chamber_glowdischarge (date,start_time, shot_no, start_pressure, start_temperature, gass) VALUES ('`date '+%y-%m-%d'`','`date '+%H:%M:%S'`',  `cat $SHM/shot_no`, `cat $SHML/ActualChamberPressuremPa`, `cat $SHML/ActualChamberTemperature`, 'He') "|ssh Dg "cat - |psql -q -U golem golem_database"
    echo "!!ENGAGE HV PS & Open gas reservoir !!"
    echo 'echo "APPL 25,2;OUTPut:IMMediate ON"|netcat -w 1 GWInstekPSW-a 2268'
    echo 'echo "APPL 55,2;OUTPut:IMMediate ON"|netcat -w 1 GWInstekPSW-a 2268'
    echo 'echo "OUTPut:IMMediate OFF"|netcat -w 1 GWInstekPSW-a 2268'
    echo 'or'
    echo 'GlowDischGasFlowSetup 65/25'
    
}    
function GlowDischGasFlowSetup ()
{
    echo "APPL $1,2;OUTPut:IMMediate ON"|netcat -w 1 GWInstekPSW-a 2268
    echo "Do not forget to run: GlowDischWaitForStop"
}    
    
    

function GlowDischWaitForStop()
{
    echo "INSERT into chamber_management (event,date,time, shot_no, session_id, chamber_pressure,  forvacuum_pressure, temperature) VALUES ('glow discharge:start','`date '+%y-%m-%d'`','`date '+%H:%M:%S'`', `cat $SHM/shot_no`, `cat $SHM/session_id`,`cat $SHML/ActualChamberPressuremPa`,`cat $SHML/ActualForVacuumPressurePa`, `cat $SHML/ActualChamberTemperature`) "|ssh Dg "$psql_password;cat - |psql -q -U golem golem_database"
    Time=15
    for i in `seq 1 $Time`; do echo Waiting for GD to stop $i/$Time; sleep 1m;done
    GlowDischStop
    
}    

function GlowDischStop()
{
    $LogFunctionPassing;
    echo "OUTPut:IMMediate OFF"|netcat -w 1 GWInstekPSW-a 2268
    sleep 10s
    GlowDischPSOFF
    TMP1StandbyOFF
    TMP2StandbyOFF
    sleep 5s
    Vent1ON
    GasHeOFF
    echo "!!Close He reservoir !!"
    echo "INSERT into chamber_management (event,date,time, shot_no, session_id, chamber_pressure,  forvacuum_pressure, temperature) VALUES ('glow discharge:end','`date '+%y-%m-%d'`','`date '+%H:%M:%S'`', `cat $SHM/shot_no`, `cat $SHM/session_id`,`cat $SHML/ActualChamberPressuremPa`,`cat $SHML/ActualForVacuumPressurePa`, `cat $SHML/ActualChamberTemperature`) "|ssh Dg "$psql_password;cat - |psql -q -U golem golem_database"
    #echo "UPDATE chamber_glowdischarge SET end_time='`date +%H:%M:%S`',end_pressure=`cat $SHML/ActualChamberPressuremPa`,end_temperature=`cat $SHML/ActualChamberTemperature`  WHERE id IN(SELECT max(id) FROM chamber_glowdischarge)"|ssh Dg "cat - |psql -q -U golem golem_database"

    
    
}    

function Baking_ON(){
    $LogFunctionPassing;
    # ChamberGraph &
     echo "INSERT into chamber_management (event,date,time, shot_no, session_id, chamber_pressure,  forvacuum_pressure, temperature) VALUES ('baking:start','`date '+%y-%m-%d'`','`date '+%H:%M:%S'`', `cat $SHM/shot_no`, `cat $SHM/session_id`,`cat $SHML/ActualChamberPressuremPa`,`cat $SHML/ActualForVacuumPressurePa`, `cat $SHML/ActualChamberTemperature`) "|ssh Dg "$psql_password;cat - |psql -q -U golem golem_database"
    #echo "INSERT into chamber_baking (date,start_time, shot_no, start_pressure, start_temperature) VALUES ('`date '+%y-%m-%d'`','`date '+%H:%M:%S'`',  `cat $SHM/shot_no`, `cat $SHML/ActualChamberPressuremPa`, `cat $SHML/ActualChamberTemperature`) "|ssh Dg "cat - |psql -q -U golem golem_database"
    Charger EtCommutatorOFF
    sleep 1
    BakingON
}

function Baking_OFF(){
    $LogFunctionPassing;
    BakingOFF
    echo "INSERT into chamber_management (event,date,time, shot_no, session_id, chamber_pressure,  forvacuum_pressure, temperature) VALUES ('baking:end','`date '+%y-%m-%d'`','`date '+%H:%M:%S'`', `cat $SHM/shot_no`, `cat $SHM/session_id`,`cat $SHML/ActualChamberPressuremPa`,`cat $SHML/ActualForVacuumPressurePa`, `cat $SHML/ActualChamberTemperature`) "|ssh Dg "$psql_password;cat - |psql -q -U golem golem_database"
    #echo "UPDATE chamber_baking SET end_time='`date +%H:%M:%S`',end_pressure=`cat $SHML/ActualChamberPressuremPa`,end_temperature=`cat $SHML/ActualChamberTemperature`  WHERE id IN(SELECT max(id) FROM chamber_baking)"|ssh Dg "cat - |psql -q -U golem golem_database"
}

function Baking_test()
{
    $LogFunctionStart
    Baking_ON
    sleep 1s
    Baking_OFF
    $LogFunctionEnd

}



