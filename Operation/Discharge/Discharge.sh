#!/bin/bash

source /dev/shm/golem/Commons.sh

whoami="Operation/Discharge/Discharge"
#backward compatibilty:
SUBDIR=`dirname $whoami`
ThisDev=`basename $whoami`

Drivers="uControllers/Arduino8relayModul uControllers/UniPi"

Devices="RASPs/Charger/Bt_Ecd RASPs/Chamber/WorkingGas PowerSupplies/GWInstekPSW-a/Working_Gas  Converters/Gnome232-b/PreionizationPowSup RASPs/Discharge/Trigger RelayBoards/Quido16-b/TriggerBattery Oscilloscopes/RigolMSO5204-c/Bt_Ecd_Monitor"
#PowerSupplies/RigolPS831A-a/HVsubstitute" # in case LV substitute



function PrepareDischargeEnv@SHM() {


    # Maintain Default/Actual/Submitted Parameters
    
       PrepareEnvironment@SHM $SHM0
       
   cp $SW_dir/Dirigent.sh $SW_dir/Commons.sh $SHM0/
   cp $SHM/* $SHM0/ 2>/dev/null
   chmod -R g+rwx $SHM
   rm -rf $SHM0/Production/Parameters
   mkdir -p $SHM0/Production/Parameters
   #first defaults
   cp $SW_dir/Management/DefaultParameters/* $SHMP/
   # and now actuals from command line:
   cp $SHMCLP/* $SHMP/
   date "+%y-%m-%d" > $SHM0/shot_date
   date "+%H:%M:%S" > $SHM0/shot_time

   
   mkdir -p $SHM0/Production/Parameters/SystemParameters
   cp $SW_dir/Management/SystemParameters/* $SHM0/Production/Parameters/SystemParameters/
   
   PrepareFilesToSHMs $SHM0 Analysis/Homepage



   U_bt_discharge_request=`cat $SHMP/UBt`;t_bt_discharge_request=`cat $SHMP/TBt`;U_cd_discharge_request=`cat $SHMP/Ucd`;t_cd_discharge_request=`cat $SHMP/Tcd`;preionization_request=`cat $SHMP/preionization`;gas_request=`cat $SHMP/gas`;p_working_gas_discharge_request=`cat $SHMP/pressure`; comment=`cat $SHMP/comment`;

  
    
    for Collection in SessionManagement DischargeBasicManagement DischargeManagement Everywhere OnStage_wave Others_wave Analysis Infrastructure DataProcessing; 
    do 
      echo ${!Collection} > $SHM0/Production/$Collection
    done  

        

    

    # rm $SHMP/discharge

    if [ ! -z "$UBt" ]; then U_bt_discharge_request=$UBt;fi
    if [ ! -z "$TBt" ]; then t_bt_discharge_request=$TBt;fi
    if [ ! -z "$Ucd" ]; then U_cd_discharge_request=$Ucd;fi
    if [ ! -z "$tcd" ]; then t_cd_discharge_request=$Tcd;fi
    if [ ! -z "$preionization" ]; then preionization_request=$preionization;fi
    if [ ! -z "$gas" ]; then gas_request=$gas;fi
    if [ ! -z "$pressure" ]; then p_working_gas_discharge_request=$pressure;fi

    # Check if parameters are within allowed range
    if [ $U_bt_discharge_request -gt 1300 ]; then EchoItColor 1 "UBt over limit ($U_bt_discharge_request>1300) !!"; return ;fi
    if [ $U_cd_discharge_request -gt  700 ]; then EchoItColor 1 "Ucd over limit ($U_cd_discharge_request>700) !!"; return ;fi
    if [ $p_working_gas_discharge_request -gt  100 ]; then EchoItColor 1 "pressure over limit ($p_working_gas_discharge_request>100) !!"; return ;fi 
    

        
    shot_no=$((`cat $SHM/shot_no`+1));echo $shot_no>$SHM/shot_no; echo $shot_no>$SHMS/shot_no ;
	LogItColor 6 "########################################################"
    LogItColor 6 "################# Discharge #$shot_no ##################"
    LogItColor 6 "########################################################"
    LogIt "Command Line Request: `cat $SHMS/session_mission`"
    
    #echo -n initializing_discharge > $SHML/tokamak_state

    
    #Database stuff
    #InsertCurrentShotDataBase "(shot_no, start_timestamp, pre_comment, \"U_bt_discharge_request\", \"U_cd_discharge_request\",\"X_working_gas_discharge_request\", p_working_gas_discharge_request, t_bt_discharge_request, t_cd_discharge_request, preionization_request, p_chamber_pressure_before_discharge, \"X_discharge_command\", dirigent_server_loadavg, session_id, session_mission, session_date, session_setup) VALUES ($((`cat $SHMS/shot_no`)), '`date "$date_format"`', '$comment', $U_bt_discharge_request, $U_cd_discharge_request,'$gas_request' ,$p_working_gas_discharge_request, $t_bt_discharge_request, $t_cd_discharge_request, $preionization_request, `cat $SHML/ActualChamberPressuremPa`,'`cat $SHMP/CommandLine`','`cat /proc/loadavg`',`cat $SHMS/session_id`, '`cat $SHMS/session_mission`','`cat $SHMS/session_date`','`cat $SHMS/session_setup_name`')"
    
    InsertCurrentShotDataBase "(shot_no, start_timestamp, pre_comment, \"U_bt_discharge_request\", \"U_cd_discharge_request\",\"X_working_gas_discharge_request\", p_working_gas_discharge_request, t_bt_discharge_request, t_cd_discharge_request, preionization_request, p_chamber_pressure_before_discharge, dirigent_server_loadavg, session_id, session_mission, session_date, session_setup) VALUES ($((`cat $SHMS/shot_no`)), '`date "$date_format"`', '$comment', $U_bt_discharge_request, $U_cd_discharge_request,'$gas_request' ,$p_working_gas_discharge_request, $t_bt_discharge_request, $t_cd_discharge_request, $preionization_request, `cat $SHML/ActualChamberPressuremPa`,'`cat /proc/loadavg`',`cat $SHMS/session_id`, '`cat $SHMS/session_mission`','`cat $SHMS/session_date`','`cat $SHMS/session_setup_name`')"
    
    # specific solution with single quotes:
    psql -c "UPDATE  shots SET \"X_discharge_command\"=E'`cat $SHMP/CommandLine|sed "s/'/\\\\\'/g"`' WHERE shot_no IN(SELECT max(shot_no) FROM shots)" -q -U golem golem_database

    
    
    currshot=/golem/database/operation/shots/$shot_no
    ln -s $SHM0 $currshot # we need to provide data even within the discharge temporally
    unlink /golem/database/operation/shots/0
    ln -s $currshot /golem/database/operation/shots/0
    
    #cp $SW_dir/Analysis/Homepage/DynamicIndex/* $SHM0/  # PM removed until fixing SSE


    #cp $SHM/* $SHM0/ 2>/dev/null
    #cp $SHMS/* $SHM0/ 2>/dev/null
    
    echo $shot_no>$SHM0/shot_no
    echo $comment> $SHM0/comment
    #echo -n preparing_discharge > $SHML/tokamak_state    
    
    }
    
    
 
 function FinalDischargeOperations() {

    UpdateCurrentShotDataBase "end_timestamp='`date "$date_format"`'"
    shot_no=`cat $SHM/shot_no`
    currshot=/golem/database/operation/shots/$shot_no
    unlink $currshot
    mkdir -p $currshot
    cp -r $SHM0/* $SHM0/*.* $currshot/ 1>/dev/null 2>/dev/null
    cp $SHM0/Analysis/Homepage/FinalizationWithoutAnnouncement.html $currshot/index.html
    unlink /golem/database/operation/shots/0
    ln -s $currshot /golem/database/operation/shots/0
    chmod -R g+rwx $SHM0

  

    
 }
 
  
  function Emergency() {
    Bt_Ecd_Management SecurePostDischargeState
    WorkingGasManagement SwitchOutAllGases
 }
 

    


