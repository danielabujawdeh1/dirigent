source ../../Commons.sh

locality=Diagnostics/PetiProbe
filetoreplace=Golem_analysis-CUTOFF.ipynb
script=21_DC_FastIonTemp.sh
command=PostDischargeAnalysis

do=echo #to see/test
do= #to run

  for i in `seq 36654 36670`; do
#    for i in 36670 ; do
    echo Doing ... $i;
    echo '###############################'
    echo Backup the old version ....
    $do cp $shot_dir/$i/$locality/$filetoreplace $shot_dir/$i/$locality/$filetoreplace.VersionUpTo`date '+%y%m%d'`
    $do cp $shot_dir/$i/$locality/$script $shot_dir/$i/$locality/$script.VersionUpTo`date '+%y%m%d'`
    echo Replacement act ...
    $do cp $dirigent_dir/$locality/$filetoreplace $shot_dir/$i/$locality/$filetoreplace
    $do cp $dirigent_dir/$locality/$script $shot_dir/$i/$locality/$script
    $do cp $dirigent_dir/$locality/* $shot_dir/$i/$locality/
    echo  And run a new version ...
    $do cd $shot_dir/$i/$locality/
    $do source `basename $script`;$do $command
    $do cd ~-
    echo '-------------------------------------------------------------------------------------'

  done
