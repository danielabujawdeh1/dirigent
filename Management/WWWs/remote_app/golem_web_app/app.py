import io
import json
from flask import Flask, render_template, request, redirect, url_for, session, flash, request_started, Response, send_file
from hashlib import md5
import qrcode
import datetime
from wtforms import Form, StringField, DateTimeField, IntegerField, validators
from unidecode import unidecode
from collections import OrderedDict
import logging
import base64

page_nav = OrderedDict((
    ('intro', 'Introduction'),
    ('control_room', 'Control room'),
    ('status', 'Live'),
    ('results', 'Results'),
))


# main web app object
app = Flask("golem_web_app")
app.config.from_object('golem_web_app.secret_settings')  # This file should be next to this one
app.config['site_name'] = 'GOLEM remote'
app.config['page_nav'] = page_nav
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False


# extra logging
handler = logging.FileHandler('/tmp/golem_remote.log')
app.logger.addHandler(handler)


# SQL model ORM
from .db_model import model_factory
db, Shot, AccessToken = model_factory(app)



# recommended root URI: /remote/

user_attrs = ('identification', 'access_token')

@app.route('/login/', methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        for key in user_attrs:
            session[key] = request.form[key]
        session['access_level'] = get_access_level(session['access_token'])
        if session['access_level']:
            flash('Access granted.')
            return redirect(url_for(request.form.get('next', 'index')))
        else:
            error = 'Invalid or expired access credentials. Try using the link or QR code you were provided (with the access token included in the URL) or request a new one.'
    defaults = {key: session.get(key) for key in user_attrs}
    defaults['next'] = request.args.get('next', 'index')
    defaults['access_level'] = session.get('access_level', 0)
    return render_template('login.html', error=error, **defaults)


@app.route('/logout/')
def logout():
    for key in user_attrs:
        if request.args.get(key) == 'yes':
            app.logger.debug('deleting session key %s' % key)
            del session[key]
    return redirect(url_for(request.args['next']))


def get_access_level(access_token=None):
    if access_token is None:
        access_token = session.get('access_token', '')
    token = AccessToken.query.filter(AccessToken.data==access_token).first()
    if (token is not None
        and (token.expires is None or
             token.expires >= datetime.datetime.now())):
        return token.level
    else:
        return 0

def generate_new_token(level=1, expires=None, expires_in=None, usage='temporary'):
    now = datetime.datetime.now()
    data = md5(app.config['GENERATOR_TOKEN_SALT'] + str(now)).hexdigest()[:32]
    if expires is None and expires_in is not None:
        expires = now + expires_in
    token = AccessToken(data=data, level=level, expires=expires, usage=usage, )
    db.session.add(token)
    db.session.commit()
    return token


def setup_access_level(sender, **extra):
    # copy any URL parameters to session
    for key in user_attrs:
        try:
            val = request.args[key]
        except KeyError:
            continue
        session[key] = val
    ident = session.get('identification')
    if ident == '_':            # special placeholder to erase
        del session['identification']
        ident = None
    if not ident:
        access_level = 0     # cannot access without identification
    else:
        access_level = get_access_level()
    session['access_level'] = access_level
    session.permanent = True

request_started.connect(setup_access_level, app)



@app.context_processor
def inject_active_dispatch():
    return dict(active_dispatch=request.path.strip('/'),
    )


@app.route('/')
def index():
    '''If introduction was skipped, go to control room, otherwise show introduction'''
    if request.cookies.get('skipIntroduction'):
        return redirect(url_for('control_room'))
    else:
        return redirect(url_for('intro'))


@app.route('/intro/')
def intro():
    '''Show introduction to fusion and the GOLEM tokamak'''
    return render_template('intro.html')

control_tabs_dict = OrderedDict([
    # key : (tab_name, description, label, sl_max, sl_step, choices, recommended_val)
    ('intro', ('Introduction',
               'This web interface will walk you through the process of configuring a discharge in the GOLEM tokamak. All settable values are perfectly safe. Proceed through each step by setting the desired values and then clicking the <span class="text-primary">Next</span> button. You can always go to a specific step by clicking its tab.',
               None,
               None,
               None,
               None,
               (),
               None
    )),
    ('gas', ('Working gas',
             'Set the pressure and type of the working gas from which the plasma is formed. Pressure must be high enough for plasma to form, but low enough for gas breakdown to occur.',
             'Gas type and pressure p<sub>WG</sub>',
             'mPa',
             60,
             2,
             ('Hydrogen', 'Helium'),
             20,
    )),
    ('preion', ('Preionization',
                'The neutral working gas must be first ionized in order to break down into a plasma.'
                'Using the <span class="text-danger">electron gun</span> will locally ionize the gas. Without any ionization, no plasma can form.',
                'Ionization method',
                None,
                None,
                None,
                ('Electron gun', 'No ionization'),
                None,
    )),
    ('Bt', ('Magnetic field',
            'Set the voltage on the capacitors to be discharged into the <span class="text-muted">toroidal field coils</span>. '
            'The higher the voltage, the larger the <span class="text-success">magnetic field</span> <i>confining</i> the plasma.',
            'Capacitor voltage U<sub>B<sub>t</sub></sub>',
            'V',
            1000,
            50,
            (),
            750,
    )),
    ('CD', ('Electric field',
            'Set the voltage on the capacitors to be discharged into the <span class="text-primary">primary transformer winding</span>. '
            'The higher the voltage, the larger the <span class="text-success">electric field</span> <i>creating and heating</i> the plasma.',
            'Capacitor voltage U<sub>E<sub>t</sub></sub>',
            'V',
            750,
            50,
            (),
            450,
    )),
])

tabs = control_tabs_dict.keys() + ['submit']
show_3D_args = {
    'True': True,
    'False': False,
}

@app.route('/control_room/')
def control_room():
    '''Display the control interface and the discharge queue'''
    if not session['access_level']:
        return redirect(url_for('login', next='control_room'))
    return render_template('control_room.html',
                           control_tabs_dict=control_tabs_dict,
                           tabs=tabs,
                           showX3DOM=show_3D_args.get(request.args.get('showX3DOM', 'False'), False),
    )


@app.route('/discharge_request/', methods=['POST'])
def discharge_request():
    data_json = request.form.get('parameters', '')
    if not data_json:
        return 'missing discharge parameters', 400  # TODO invalid form data code
    try:
        data = json.loads(data_json)
    except (ValueError, TypeError):
        return 'invalid JSON parameters object', 400
    app.logger.debug('discharge parameters request: %r', data)
    try:
        shot = Shot(**data)     # TODO check data a bit more, e.g. status, preion
    except TypeError:
        app.logger.exception('Shot creation failed')
        return 'Invalid parameters', 400
    shot.ip = request.remote_addr
    preceding = Shot.query.filter(Shot.status < 2).count()
    db.session.add(shot)
    db.session.commit()
    response_json = json.dumps({
        'machineActive': True,  # TODO detect/save in g machine status
        'minutesToDischarge': (preceding + 1) * 2,
        'vshotno': shot.vshotno,
    })
    return response_json        # TODO json encoding


@app.route('/status/')
def status():
    '''Display the current status of the GOLEM tokamak'''
    if session['access_level'] >= 1:
        return render_template('status.html')
    else:
        return render_template('video_status.html')


@app.route('/results/')
def results():
    '''Display results for the current user'''
    # TODO pagination
    if not session.get('identification'):
        return redirect(url_for('login', next='results'))

    shots = Shot.query.filter(
        (Shot.Identification == session.get('identification', ''))
        & (Shot.shotno > 0)
        )
    return render_template('results.html', shots=shots)


@app.route('/queue.json')
def queue():
    shots = Shot.query.filter(Shot.status < 2)
    shots_l = [s.__dict__ for s in shots]
    for s in shots_l:
        s.pop('_sa_instance_state', None)
        s['date'] = str(s['date'])
    ret = json.dumps({'queue': shots_l}, ensure_ascii=False)
    return Response(unidecode(ret), mimetype='application/json')


class GroupAccessGeneratorForm(Form):
    groupname = StringField('Group base name', [validators.Length(min=3, max=64)])
    n_groups = IntegerField('Number of groups', [validators.NumberRange(min=1)])
    event_date = DateTimeField('Date of the event', format='%Y-%m-%d')
    expire_after_n_days = IntegerField('Access expires n days the event date', [validators.NumberRange(min=1)])
    level = IntegerField('Access level', [validators.NumberRange(min=0)])
    admin_password = StringField('Access admin password')


@app.route('/generate_group_access', methods=['POST'])
def generate_group_access():
    form = GroupAccessGeneratorForm(request.form)
    if not form.validate():
        return "invalid POST data:\n{}".format(str(form.errors)), 400
    if form.admin_password.data != app.config['ACCESS_ADMIN_PASSWORD']:
        return "Wrong access admin password", 403
    expires = form.event_date.data + datetime.timedelta(days=form.expire_after_n_days.data)
    token = generate_new_token(level=form.level.data, usage=form.groupname.data, expires=expires)
    n_groups = form.n_groups.data
    groups_maxlen = len(str(n_groups))
    links = [url_for('control_room', identification='{} Group {:0{}d}'.format(
        form.groupname.data, i, groups_maxlen),
                     access_token=token.data, _external=True)
             for i in range(1, n_groups+1)]
    return '\n'.join(links)


@app.route('/generate_temporary_access')
def generate_temporary_access():
    passwd = request.args.get('access_admin_password', '')
    usage = request.args.get('usage', 'temporary')
    if passwd != app.config['ACCESS_ADMIN_PASSWORD']:
        return "Wrong access admin password", 403
    expires_in = datetime.timedelta(minutes=int(request.args.get('minutes', 30)))
    token = generate_new_token(level=request.args.get('level', 1),
                                     expires_in=expires_in, usage=usage)
    return redirect(url_for('qr_access_link', access_token=token.data, identification='_'))



@app.route('/qr_access_link.png')
def qr_access_link():
    # WARNING: requires SITE_NAME setting for absolute URL
    kwargs = {}
    for uat in user_attrs:
        v = request.args.get(uat)
        if v:
            kwargs[uat] = v
    link = url_for('control_room', _external=True, **kwargs)
    image = qrcode.make(link)
    stream = io.BytesIO()
    image.save(stream, 'PNG')
    stream.seek(0)
    img_data = base64.b64encode(stream.read())
    return render_template('qr_code.html', img_data=img_data, target_url=link)

