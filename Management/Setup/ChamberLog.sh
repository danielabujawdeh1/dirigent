ssh -Y golem@Chamber tail -f /dev/shm/golem/ChamberLog|feedgnuplot --terminal "x11" --geometry "1080x400-0-0" --timefmt "%H:%M:%S" --set "format x \"%H:%M\"" --legend 0 "pressure" --legend 1 "temperature" --y2min 0 --y2max 200 --ymin 0 --ymax 30 --domain --lines --y2 1 --extracmds "set term x11 1 noraise;set title \"Chamber\";set xlabel \"Time [h:m]\";set ylabel \"Pressure p_{ch} [mPa]\"; set y2label \"Temperature T_{ch} [^o C]\"" --stream &
sleep 5
wmctrl -i -r `wmctrl -l|grep 'Gnuplot'|awk '{print $1}'` -b add,sticky
wmctrl -i -r `wmctrl -l|grep 'Gnuplot'|awk '{print $1}'` -e 0,1366,1539,1080,400
#killall feedgnuplot
