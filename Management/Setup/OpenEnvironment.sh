kate -n /golem/Dirigent/Dirigent.sh /golem/Dirigent/Commons.sh /golem/Dirigent/current.setup &
google-chrome --password-store=basic --user-data-dir=$HOME/.config/google-chrome-Dirigent --new-window http://golem.fjfi.cvut.cz/shots/0 http://golem.fjfi.cvut.cz/wiki/ &
konsole --title "Dirigent" --profile Dirigent --tabs-from-file /golem/Dirigent/Management/Miscs/DirigentTabs & 
krusader --left /golem/Dirigent --right /golem/shm_golem/  &
ssh -Y golem@golem "cd /golem/Dirigent/;source Operation/Session/Controlling.sh;BasicManagementCore" &
sleep 5
# ZATIM NEFUNGUJE: xterm -hold -e 'nohup bash /golem/Dirigent/Management/Setup/Monitors/basic.xrandr'
#xterm -e 'xrandr --output HDMI-1 --primary --mode 2048x1152 --output DVI-I-2-2 --mode 1920x1080 --above HDMI-1 --rotate right --output DVI-I-1-1 --mode 1366x768 --right-of HDMI-1'
#sleep 5
# Move controll panel to the left display
wmctrl -i -r `wmctrl -l|grep 'Dirigent@Operator'|awk '{print $1}'` -b add,sticky
wmctrl -i -r `wmctrl -l|grep 'Dirigent@Operator'|awk '{print $1}'` -e 0,80,2000,1200,250

# Stay chrome on the main display
wmctrl -i -r `wmctrl -l|awk '{if ( $2 == 1 ){print $0}}'|grep Chrome|awk '{print $1}'` -e 0,1366,1920,2048,1052

