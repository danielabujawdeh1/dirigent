      <div class="sphinxsidebar">
	<div class="sphinxsidebarwrapper" style="position:fixed;z-index:1;float: left; margin-right: 0px; width: 172px;">


	    <a href="http://golem.fjfi.cvut.cz/shots/<?php echo $shot_no;?>"><img src="/_static/logos/golem.svg" alt="" width="80"></a>

		<h4>Diagnostics</h4>
		<ul>
		
				   <?php
    $dir='Diagnostics';
    system ("for i in `ls /golem/database/operation/shots/$shot_no/$dir/`; do echo '<li><a href=http://golem.fjfi.cvut.cz/shots/$shot_no/$dir/'\$i'/onstage.html>'\$i'</a></li>';done");
    ?>
		</ul>


		<h3>Other</h3>
		<ul class="this-page-menu">
		
        
    
 <!--<li><a class="reference internal" href="http://golem.fjfi.cvut.cz/shots/<?php echo $shot_no;?>/Data.php">Data</a></li>-->
 <!--<li><a class="reference internal" href="http://golem.fjfi.cvut.cz/wiki/Bibliography.pdf">References</a></li>-->
 <!--<li><a class="reference internal" href="http://golem.fjfi.cvut.cz/shots/<?php echo $shot_no;?>/About.php">About</a></li>-->
 <li><a class="reference internal" href="http://golem.fjfi.cvut.cz/wiki/">Wiki</a></li>
 <li><?php echo $underconst; ?><a class="reference internal" href="Analysis/Homepage/bookmarks.html">"Showroom"</a>
  </li>
 <!--<li><a class="reference internal" href="http://golem.fjfi.cvut.cz/utils/">Utilities</a></li>-->

		</ul>

		<h4>Navigation</h4>
		<ul class="this-page-menu">
  
 <li>
    
    <a class="reference internal" href="http://golem.fjfi.cvut.cz/shots/<?php echo ($shot_no+1);?>">Next</a>
    </li>
  
 <li>
    
    <a class="reference internal" href="http://golem.fjfi.cvut.cz/shots/<?php echo ($shot_no-1);?>">Previous</a>
    </li>
<li>
    
    <a class="reference internal" href="http://golem.fjfi.cvut.cz/shots/0">Current</a>
    </li>

		</ul>


	    <div id="searchbox" style="">
		<h3>Go to shot</h3>
		    <form style="display:inline;" method="post" action="http://golem.fjfi.cvut.cz/shots/<?php echo $shot_no;?>/" onsubmit="return getURL(this.shot.value)">
		    <input size="5" type="text" name="shot" value="<?php echo ($shot_no);?>">
		    <input size="5" type="submit" name="Go" value="Go">
		    </form>
	    </div>
	    <script type="text/javascript">$('#searchbox').show(0);</script>

        <div id="searchbox">
            <br>
            <h3>Golem utils</h3>
            <ul class="this-page-menu">
                <li> <a class="reference internal" href="https://golem.fjfi.cvut.cz/plot"> Home </li>
                <li> <a class="reference internal" href="https://golem.fjfi.cvut.cz/plot/select_shot"> Plot data </li>
                <li> <a class="reference internal" href="https://golem.fjfi.cvut.cz/plot/interval_select"> Shot interval plot </li>
                <li> <a class="reference internal" href="https://golem.fjfi.cvut.cz/plot/manipulators"> Manipulators control </li>
            </ul>
        </div>
	</div>
