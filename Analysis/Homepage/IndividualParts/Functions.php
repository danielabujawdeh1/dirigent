<?php

echo "\(\def\sub#1{_\mathrm{#1}}\def\sup#1{^\mathrm{#1}}\def\maximum#1{max_#1}\)";

#$shot_no=exec ('echo "SELECT shot_no FROM shots ORDER BY shot_no  DESC LIMIT 1;"|psql -qAt -U golem golem_database'); #obsolete
#$shot_no=33913; # tuning purposes
$shot_no=exec ('cat ../../shot_no');


#WWW stuff:
$linkiconsize="20px";
$smalllinkiconsize="17px";
$icon_size="200x150";
$ScreenShotAllSize="1600,1000";
$namesize=100;
$iconsize=200;
$imgpath="http://golem.fjfi.cvut.cz/_static";
$gitlabpath="https://gitlab.com/golem-tokamak/dirigent/-/tree/master";
$gitlabicon="<img src=$imgpath/gitlab.png  width='$linkiconsize'/>";
$pythonicon="<img src=$imgpath/python.png  width='$linkiconsize'/>";
$resultsicon="<img src=$imgpath/results.png  width='$linkiconsize'/>";
$diricon="<img src=$imgpath/direct.png  width='$linkiconsize'/>";
$gnuploticon="<img src=$imgpath/gnuplot.png  width='$linkiconsize'/>";
$rightarrowicon="<img src=$imgpath/rightarrow.png  width='10px'/>";
$dbpath="http://golem.fjfi.cvut.cz/dbase";
$psqlicon="<img src=$imgpath/postgresql.webp  width='$smalllinkiconsize'/>";
$underconst="<img src=$imgpath/UnderConstruction.png  width='$linkiconsize'/>";
//$psql_password=`cat /golem/production/psql_password`;
$psql_password='export PGPASSWORD='.file_get_contents('/golem/production/psql_password');

function CurrentStatusAnnouncement ($status, $next)
{
echo "<h2 style='background-color: bisque; color: brown;'><center>Right now the tokamak GOLEM is undergoing discharge execution, the phase called:</center></h2>
<h2 style='background-color: brown; color: bisque;'><center><b>$status</b></center></h2>
<h2 style='background-color: bisque; color: brown;'><center>Next phase will be <b>$next</b>.</center></h2>
<h2 style='background-color: bisque; color: brown;'><center>Wait for the discharge proccess to be completed.</center></h2>";
}

function StandbyAnnouncement ()
{
echo "<h2 style='background-color: bisque; color: brown;'><center>The tokamak GOLEM is now in the Standby mode.</center></h2>";
}

function printf_psql_shots_request ($request, $format)
{
global $shot_no;
global $psql_password;
echo exec ($psql_password.'echo "SELECT \"'.$request.'\" FROM shots WHERE shot_no='.$shot_no.';"|psql -qAt -U golem golem_database|xargs printf \''.$format.'\';');
}

function psql_shots_request ($request)
{
global $shot_no;
global $psql_password;
echo exec ($psql_password.'echo "SELECT \"'.$request.'\" FROM shots WHERE shot_no='.$shot_no.';"|psql -qAt -U golem golem_database;');
}

function bare_psql_shots_request ($request)
{
global $shot_no;
global $psql_password;
return exec ($psql_password.'echo "SELECT \"'.$request.'\" FROM shots WHERE shot_no='.$shot_no.';"|psql -qAt -U golem golem_database;');
}


function psql_physical_quantities_request ($column,$identifier)
{
global $psql_password;
echo exec ($psql_password.'echo "SELECT '.$column.' FROM physicalquantities WHERE identifier=\''.$identifier.'\';"|psql -qAt -U golem golem_database');
}


function printf_quantity_item ($request,$format)
{
    echo  "<tr class='field'><th class='field-name' style='white-space: nowrap;'>";
    psql_physical_quantities_request ('description',$request);
    echo "</th><td class='field-body'>";
    printf_psql_shots_request ($request,$format).'</td></tr>';

}

function quantity_item ($request)
{
    echo  "<tr class='field'><th class='field-name' style='white-space: nowrap;'>";
    psql_physical_quantities_request ('description',$request);
    echo "</th><td class='field-body'>";
    psql_shots_request ($request).'</td></tr>';

}

function href_quantity_item ($request,$href)
{
    echo  "<tr class='field'><th class='field-name' style='white-space: nowrap;'>";
    echo "<a href=$href>";
    psql_physical_quantities_request ('description',$request);
    echo "</a></th><td class='field-body'>";
    psql_shots_request ($request);
    echo " <i class='fa fa-copy' onclick='textToClipboard()' style='font-size:20px'></i>".'</td></tr>';

}


function printf_parameters_quantity_item ($request,$format)
{
    echo  "<li>";
    psql_physical_quantities_request ('description',$request);
    echo  ": ";
    printf_express_quantity ($request,$format);
    echo "</li>";
}

function printf_express_quantity ($request,$format)
{
    echo "\(";psql_physical_quantities_request ('latex',$request);echo "\)=";
    printf_psql_shots_request ($request,$format);
    echo " ";
    psql_physical_quantities_request ('unit',$request);
}

function printf_href_express_quantity ($address,$request,$format)
{
    echo "<a href=$address/$request title=\"";
    psql_physical_quantities_request ('description',$request);
    echo "\">\(";psql_physical_quantities_request ('latex',$request);echo "\)=";
    printf_psql_shots_request ($request,$format);
    echo " ";
    psql_physical_quantities_request ('unit',$request);
    echo "</a>";
}

?>

<?php
global $iconsize;
$tablehead='<table>
<tbody>
<tr class="data-flow">
<th>Data flow</th><th colspan="2">measurement &rarr;</th><th colspan="2" >digitization &rarr;</th><th colspan="2">analysis &rarr;</th><th colspan="2"></th></tr>

<tr>
<th>Name</th><th colspan="2">Experiment setup</th><th colspan="2">Data acquistition system</th><th colspan="2">Raw data</th><th colspan="2">Analysis results</th></tr>';
$tableend='</tbody></table>';

?>
