<h2>&nbsp;&nbsp;&nbsp;&nbsp;
<a href='Infrastructure/'>Technological parameters </a>
<a href="<?php echo $gitlabpath; ?>/Devices/RASPs"><?php echo $gitlabicon; ?></a>  
</h2>

<ul>
    <?php 
    echo "<li><a href='Devices/RASPs/Chamber/'>Working Gas</a>: ";
    printf_href_express_quantity('Devices/RASPs/Chamber','p_chamber_pressure_before_discharge','%3.2f');
    echo "; ";
    printf_href_express_quantity('Devices/RASPs/Chamber','p_chamber_pressure_predischarge','%3.2f');
    echo " (";
    printf_href_express_quantity('Devices/RASPs/Chamber','p_working_gas_discharge_request','%3.0f');
    echo "@";
    printf_href_express_quantity('Devices/RASPs/Chamber','X_working_gas_discharge_request','%s');
    echo ")";
    echo "</li>";
    echo "<li><a href='Devices/RASPs/Charger/'>Toroidal magnetic field</a>: ";
    printf_href_express_quantity('Devices/RASPs/Charger','U_bt_discharge_request','%3.0f');
    echo "@";
    printf_href_express_quantity('Devices/RASPs/Discharge','t_bt_discharge_request','%3.1f');
    echo "</li>";
    echo "<li><a href='Devices/RASPs/Charger/'>Current drive field</a>: ";
    printf_href_express_quantity('Devices/RASPs/Charger','U_cd_discharge_request','%3.0f');
    echo "@";
    printf_href_express_quantity('Devices/RASPs/Discharge','t_cd_discharge_request','%3.1f');
    echo "</li>";
//     parameters_quantity_item ('preionization_request');
        ?>
    </ul>