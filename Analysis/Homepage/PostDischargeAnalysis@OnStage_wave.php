<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<?php
/*bring our bash variables to PHP*/
$config = parse_ini_file("../../session.setup", true);
extract($config);
?>

<?php include 'IndividualParts/Functions.php'; ?>
<?php include 'IndividualParts/Head.php'; ?>
<body>
<?php include 'IndividualParts/Navigation.php'; ?>
<?php include 'IndividualParts/DischargeHeading.php'; ?>  

<table><tbody><tr valign="top">
<td><?php include 'IndividualParts/TechnologicalParameters.php'; ?></td>
<td><?php include 'IndividualParts/PlasmaParameters.php'; ?></td>
</tr></tbody></table>


<?php include 'IndividualParts/BasicDiagnostics.php'; ?>
<?php CurrentStatusAnnouncement("ON STAGE postdischarge analysis", "OFF STAGE postdischarge analysis"); ?>

<?php include 'IndividualParts/Foot.php'; ?>
<?php include 'IndividualParts/SideBar.php'; ?>
</body></html>

