<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <style>
    html, body, {
    position:fixed;
    top:0;
    bottom:0;
    left:0;
    right:0;
  }
  .header {
			position: absolute;
			top: 0;
			left: 0;
      padding: 0vw;
      text-align: center;
      background: grey;
      color: white;
      font-size: 0.75vw;
      height: 3vw;
			width: 100vw;
    }
    #listing{
      top: 3.5vw;
      height: 45vw;
      overflow: scroll;
      position: absolute;
      width: 50vw;
      padding: 0.5vw 3vw;
      border: 0.02vw solid black;
      box-shadow: 0 1vw 2vw 0 rgba(0,0,0,0.1),
        0 0.33vw 1vw 0 rgba(0,0,0,0.78);
      font-size: 0.8vw;
      scroll-behavior: smooth;
    }
    #result{
      overflow: scroll;
      position: absolute;
      text-align: center;
      padding: 0.5vw 3vw;
      width: 33vw;
      height: 45vw;
      top: 3.5vw;
      font-size:1.5vw;
      left: 30vw;
      border: 1px solid black;
      box-shadow: 0 1vw 2vw 0 rgba(0,0,0,0.1),
        0 0.33vw 1vw 0 rgba(0,0,0,0.78);
    }
    ul {
      column-count: 2;
      -webkit-columns: 2;
      -moz-columns: 2;
      flex-direction: column;
    }
    ul li{
      word-wrap: break-word;
      padding: 0vw 0.5vw;
    }

    /* width */
  ::-webkit-scrollbar {
    width: 0.5vw;
  }

  /* Track */
  ::-webkit-scrollbar-track {
    background: #f1f1f1;
  }

  /* Handle */
  ::-webkit-scrollbar-thumb {
    background: #888;
  }

  /* Handle on hover */
  ::-webkit-scrollbar-thumb:hover {
    background: #555;
  }
  </style>
</head>

<body>
  <div class="wrapper">
  <div class="header">
    <h1>Tokamak GOLEM discharge preparation webpage</h1>
  </div>
  <!-- <div id="listing"></div> --!>
  <div id="result"></div>
  </div>
  <!-- <script>
  if(typeof(EventSource) !== "undefined") {
    var source = new EventSource("files.php");
    source.onmessage = function(event) {
      document.getElementById("listing").innerHTML = event.data + "<br>";
    };
  } else {
    document.getElementById("listig").innerHTML = "Sorry, your browser does not support server-sent events...";
  }
  </script> --!>




  <script>
  if(typeof(EventSource) !== "undefined") {
    var source = new EventSource("status.php");
    source.onmessage = function(event) {
      document.getElementById("result").innerHTML = event.data + "<br>";
			if (document.getElementById("result").innerHTML.indexOf("SleepOnLan@Everywhere") != -1 ||  document.getElementById("result").innerHTML.indexOf("idle") != -1) {
					location.reload();
			 }
    };
  } else {
    document.getElementById("result").innerHTML = "Sorry, your browser does not support server-sent events...";
  }
  </script>

</body>
