#!/bin/bash
BASEDIR="../../"
source $BASEDIR/Commons.sh



function MakeScan()
{

  start=$(for i in `cat ../../Production/Parameters/ScanDefinition|sed 's/,/ /g' `; do echo $i;done|head -1);
  end=$(cat ../../shot_no);
  shot_no=$(cat ../../shot_no);

  mkdir -p Scan;  rm Scan/* 
  echo -n "ShotPath='< wget -q -O - http://golem.fjfi.cvut.cz/shots';DataId='Diagnostics/BasicDiagnostics/Results/U_loop.csv';set datafile separator ',';set key left;set xlabel 'Time [s]';set ylabel 'U_{loop} [V]';set title 'U_{loop}' ;plot" > Scan/Uloopscript.gp
  echo -n "ShotPath='< wget -q -O - http://golem.fjfi.cvut.cz/shots';DataId='Diagnostics/BasicDiagnostics/Results/Ip.csv';set datafile separator ',';set key left;set xlabel 'Time [s]';set ylabel 'I_{p} [kA]';set title 'I_{p}' ;plot" > Scan/Ipscript.gp
  echo -n "DataPath='< wget -q -O - http://golem.fjfi.cvut.cz/shots/$shot_no/Analysis/Homepage/Scan/';set datafile separator ',';set key enhanced;set xlabel 'ShotNo [#]';set bmargin 1;set tmargin 0;set lmargin 5;set rmargin 5;unset xlabel;set multiplot layout 4,2 columnsfirst title '';set xrange [$((start-1)):$((end+1))];set yrange [*:*];set style data dots;"> Scan/Valuesscript.gp
  #  cat tmp|gnuplot 1>/dev/null 2>/dev/null
  #  convert -resize 200x200 ScreenShotAll.png rawdata.jpg

  for i in `cat ../../Production/Parameters/ScanDefinition|sed 's/,/ /g' ` `cat ../../shot_no`; do 
    echo -n " ShotPath.'/$i/'.DataId u 1:2 w l title '$i'," >>Scan/Uloopscript.gp
    echo -n " ShotPath.'/$i/'.DataId u 1:2 w l title '$i'," >>Scan/Ipscript.gp
    for j in Charger/U_bt_discharge_request Charger/U_cd_discharge_request Chamber/p_working_gas_discharge_request Discharge/t_cd_discharge_request;do
      echo -n "$i," >> Scan/`basename $j`.csv; wget -q -O - http://golem.fjfi.cvut.cz/shots/$i/Devices/RASPs/$j >> Scan/`basename $j`.csv;echo "" >> Scan/`basename $j`.csv 
    done

    for j in t_plasma_duration U_loop_mean Ip_max Bt_max;do
      echo -n "$i," >> Scan/$j.csv; wget -q -O - http://golem.fjfi.cvut.cz/shots/$i/Diagnostics/BasicDiagnostics/Results/$j >> Scan/$j.csv;echo "" >> Scan/$j.csv 
    done
  done
  
  echo  "set terminal postscript enhanced color;"|cat - Scan/Uloopscript.gp |gnuplot > Scan/U_loop.eps
  echo  -n "echo \""|cat - Scan/Uloopscript.gp > Scan/UloopScript.gp;echo "\" |gnuplot --persist" >> Scan/UloopScript.gp
  gs -dNOPAUSE -r300 -sDEVICE=jpeg -dBATCH -sOutputFile=Scan/U_loop.jpg Scan/U_loop.eps
  mogrify -rotate 90 Scan/U_loop.jpg
  echo  "set terminal postscript enhanced color;"|cat - Scan/Ipscript.gp |gnuplot > Scan/I_p.eps
  echo  -n "echo \""|cat - Scan/Ipscript.gp > Scan/IpScript.gp;echo "\" |gnuplot --persist" >> Scan/IpScript.gp

  #gnuplot -c Scan/Ipscript.gp > Scan/I_p.eps
  gs -dNOPAUSE -r300 -sDEVICE=jpeg -dBATCH -sOutputFile=Scan/I_p.jpg Scan/I_p.eps
  mogrify -rotate 90 Scan/I_p.jpg
  
  PointSize=2
  for j in "U_bt_discharge_request U^{req}_{B_t} V" "U_cd_discharge_request U_{E_{cd}}^{req} V" "p_working_gas_discharge_request p_{WG}^{req} mPa";do
      tmp=($j);
      echo -n "unset xtics;set ylabel '${tmp[1]} [${tmp[2]}]';plot DataPath.'/${tmp[0]}.csv' u 1:2 t '' w p ps $PointSize;" >> Scan/Valuesscript.gp
  done
  for j in "t_cd_discharge_request  t_{B_t}^{req} us";do
      tmp=($j);
      echo -n "set xtics;;set xtics rotate;set ylabel '${tmp[1]} [${tmp[2]}]';plot DataPath.'/${tmp[0]}.csv' u 1:2 t '' w p ps $PointSize;" >> Scan/Valuesscript.gp
  done
  for j in "t_plasma_duration t_p ms" "Ip_max I_p^{max} kA" "Bt_max B_t^{max} T";do
      tmp=($j);
      echo -n "unset xtics;set ylabel '${tmp[1]} [${tmp[2]}]';plot DataPath.'/${tmp[0]}.csv' u 1:2 t '' w p ps $PointSize;" >> Scan/Valuesscript.gp
  done
  for j in U_loop_mean;do
      echo -n "set xtics;set xtics rotate;set ylabel 'U_{l}^{mean} [V]';plot DataPath.'/$j.csv' u 1:2 t '' w p ps $PointSize;" >> Scan/Valuesscript.gp
  done
  
  echo  "set terminal postscript enhanced color;"|cat - Scan/Valuesscript.gp |gnuplot > Scan/Values.eps
  echo  -n "echo \""|cat - Scan/Valuesscript.gp > Scan/ValuesScript.gp;echo "\" |gnuplot --persist" >> Scan/ValuesScript.gp
  gs -dNOPAUSE -r300 -sDEVICE=jpeg -dBATCH -sOutputFile=Scan/Values.jpg Scan/Values.eps
  mogrify -rotate 90 Scan/Values.jpg
    
    echo "<br></br><br></br><table border=1>" >Scan/diags.html
    echo "<tr>" >> Scan/diags.html
    for j in `cat ../../Production/OnStage_wave`; do
	echo "<td width=20% colspan=2 align='center'><img src=/shots/$i/`dirname $j`/name.png style=transform:rotate(90deg)></img></td>" >> Scan/diags.html
    done
    echo "</tr><tr>" >> Scan/diags.html
    for j in `cat ../../Production/OnStage_wave`; do
	echo "<td width=10% align='center'><img src=/shots/$i/`dirname $j`/DAS_raw_data_dir/das.jpg width=$iconsize ></img></td>" >> Scan/diags.html
	echo "<td width=10% align='center'><a href=/shots/$i/`dirname $j`/expsetup.svg><img src=/shots/$i/`dirname $j`/setup.png></img></a></td>" >> Scan/diags.html
      done
	echo "</tr>" >> Scan/diags.html
    
    
   for i in `cat ../../Production/Parameters/ScanDefinition|sed 's/,/ /g' ` `cat ../../shot_no`; do 
       echo "<tr><td colspan=100%><a href=/shots/$i>#$i</a>&nbsp;&nbsp;&nbsp;&nbsp `cat /golem/database/operation/shots/$i/Production/Parameters/CommandLine`</td></tr><tr>" >>Scan/diags.html
      for j in `cat ../../Production/OnStage_wave`; do
	echo "<td><a href=/shots/$i/`dirname $j`/DAS_raw_data_dir/ScreenShotAll.png><img src=/shots/$i/`dirname $j`/DAS_raw_data_dir/rawdata.jpg></img></a></td>" >> Scan/diags.html
	echo "<td><a href=/shots/$i/`dirname $j`/analysis.html><img src=/shots/$i/`dirname $j`/graph.png></img></a></td>" >> Scan/diags.html
      done
      echo "</tr>" >>Scan/diags.html
   done
   echo "</table>" >> Scan/diags.html

}

