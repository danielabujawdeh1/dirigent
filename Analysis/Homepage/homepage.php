<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<?php
/*bring our bash variables to PHP*/
$config = parse_ini_file("../../session.setup", true);
extract($config);
?>


<?php include 'IndividualParts/Functions.php'; ?>
<?php include 'IndividualParts/Head.php'; ?>
<body>
<?php include 'IndividualParts/Navigation.php'; ?>
<div style="display:grid;grid-template-columns: repeat(2, 2fr);">
<div>
    <?php include 'IndividualParts/DischargeHeading.php'; ?>

    <table><tbody><tr valign="top">
    <?php include 'IndividualParts/TechnologicalParameters.php'; ?>
    <?php include 'IndividualParts/PlasmaParameters.php'; ?>
    </tr></tbody></table>
</div>
<div>
<?php
//     if (!empty($BasicDiagnostics)) {
        echo "<H1>Basic Diagnostics &nbsp;
        <a href=".$gitlabpath."/Diagnostics/BasicDiagnostics/DetectPlasma.ipynb title=\"Python code@Jupyter notebook\">".$pythonicon."</a>".$rightarrowicon." <a href=Diagnostics/BasicDiagnostics/DetectPlasma.html title=\"Results\">".$resultsicon."</a>
        ".$rightarrowicon."
        <a href=".$gitlabpath."/Diagnostics/BasicDiagnostics/StandardDAS.ipynb title=\"Python code@Jupyter notebook\">".$pythonicon."</a>".$rightarrowicon."<a href=Diagnostics/BasicDiagnostics/analysis.html title=\"Results\">".$resultsicon."</a>&nbsp;<a href=Diagnostics/BasicDiagnostics/Results title=\"Results\">".$diricon."</a>
        </H2>";
        echo "<div style='text-align:center;'><iframe src='http://golem.fjfi.cvut.cz/shots/$shot_no/Diagnostics/BasicDiagnostics/homepage_figure.html' width=660 height=650 frameBorder='0' ></iframe></div>";
//         echo "$tablehead";
//         $output=shell_exec('cat ../../Diagnostics/BasicDiagnostics/diagrow.html');
//         echo "$output";
//         echo "$tableend";

//         }
?>
</div>
</div>



<?php
global $iconsize;
$tablehead='<table>
<tbody>
<tr class="data-flow">
<th>Data flow</th><th colspan="2">measurement &rarr;</th><th colspan="2" >digitization &rarr;</th><th colspan="2">analysis &rarr;</th><th colspan="2"></th></tr>

<tr>
<th>Name</th><th colspan="2">Experiment setup</th><th colspan="2">Data acquistition system</th><th colspan="2">Raw data</th><th colspan="2">Analysis results</th></tr>';
$tableend='</tbody></table>';

?>

<h2>On stage diagnostics</h2>




<!--<table>
<tbody><tr>-->

    <?php
    global $iconsize;
    echo "$tablehead";
    system ("for i in `cat ../../Production/OnStage_wave`; do cat ../../`dirname \$i`/diagrow_`basename \$i`.html;done");
    echo "$tableend";
    ?>

    <?php
    if (filesize ('../../Production/Others_wave' ) > 10) {
    echo "<h2>Other diagnostics</h2>";
    global $iconsize;
    echo "$tablehead";
    system ("for i in `cat ../../Production/Others_wave`; do cat ../../`dirname \$i`/diagrow_`basename \$i`.html;done");
    echo "$tableend";
    }
    ?>
    
 
    



    <?php
    if (bare_psql_shots_request('is_plasma') != 0) {
//    if (filesize ('../../Production/Analysis' ) > 10) {
    global $iconsize;
    echo "<h2>Analysis</h2><table><tr>
<th>Name</th><th colspan='2'>Analysis results</th></tr>";
    system ("for i in `cat ../../Production/Analysis`; do cat ../../`dirname \$i`/diagrow_`basename \$i`.html;done");
    echo "</table>";
//     }
}
    ?>
    
       <?php
//     if (bare_psql_shots_request('is_plasma') != 0) {
    if (filesize ('../../Production/DataProcessing' ) > 2) {
    global $iconsize;
    system ("for i in `cat ../../Production/DataProcessing`; do cat ../../`dirname \$i`/diagrow.html;done");
    echo "</table>";
//     }
}
    ?>

    

    <?php
        if (filesize ('../../Production/Infrastructure' ) > 10) {
     echo "<h2>Infrastructure</h2><table>";
    system ("for i in `cat ../../Production/Infrastructure`; do cat ../../`dirname \$i`/include.html;done");
    echo "</table>";
    }
    ?>

    
    
<?php
$scan_def=exec ('cat ../../Production/Parameters/ScanDefinition');
     if (!empty($scan_def)) {
        echo "<H2>Scan issues <a href=https://gitlab.com/golem-tokamak/dirigent/-/tree/master/Analysis/Homepage title='Gitlab repo' >$gitlabicon</a><a href=Analysis/Homepage/Scan/ title='Directory with data'>$diricon</a></H2>";
        echo "<i>for ";
        echo file_get_contents('../../Production/Parameters/ScanDefinition', true);
        echo " + ";
        echo file_get_contents('../../shot_no', true);
        echo "</i><br></br><center>";
        echo "<a href=/shots/$shot_no/Analysis/Homepage/Scan/Values.jpg><img src=/shots/$shot_no/Analysis/Homepage/Scan/Values.jpg width='60%' height='25%'></img></a>
        <a href=/shots/$shot_no/Analysis/Homepage/Scan/ValuesScript.gp>$gnuploticon</a>
        <br></br>";
        echo "<a href=/shots/$shot_no/Analysis/Homepage/Scan/U_loop.jpg><img src=/shots/$shot_no/Analysis/Homepage/Scan/U_loop.jpg width=45% ></img></a>
        <a href=/shots/$shot_no/Analysis/Homepage/Scan/UloopScript.gp>$gnuploticon</a>";
        echo "<a href=/shots/$shot_no/Analysis/Homepage/Scan/Values.jpg><img src=/shots/$shot_no/Analysis/Homepage/Scan/I_p.jpg width=45% ></img></a>
        <a href=/shots/$shot_no/Analysis/Homepage/Scan/IpScript.gp>$gnuploticon</a>";
        $file = file_get_contents('Scan/diags.html', true);
        echo $file;
        }
?>
    
    

<?php
//     $output=shell_exec('cat ../../analysis_wave_i/DataProcessing/DataProcessing.html');
//     echo "$output";
?>







<h2> Acknowledgement  </h2>

<table cellpadding="5">
<tbody><tr>
<td valign="middle">
<a href="http://jaderka.cz/"><img src="/_static/logos/fjfi.svg" width="80px"></a>
</td>
<td valign="middle" align="center">
<div style="display:table-cell; vertical-align:middle">
<a href="http://www.fusenet.eu/"><img src="/_static/logos/fusenet_logo.gif" width="150px"></a>
</div>
</td>
<td valign="middle">
<a href="https://www.euro-fusion.org/"><img src="/_static/logos/eurofusion.png" width="150px"></a>
</td>
<td valign="middle">
<a href="https://www.iaea.org/"><img src="/_static/logos/iaea.png" width="80px"></a>
</td>

</tr>
</tbody></table>

<hr>

Thanks to the specific help
<span style="display:inline-block; vertical-align:middle">
<a href="http://www.pfeiffer-vacuum.com/"><img src="/_static/logos/Pfeiffer_Vacuum_Technology.svg" width="150px"></a></span>

<p style="text-align:right;">
<span style="display:inline-block; vertical-align:middle; horizontal-align:right">
<a href="https://gitlab.com/golem-tokamak/dirigent">The tokamak GOLEM controll SW @ <img src="/_static/logos/GitLab.png" width="70px"></a></span></p>

	    </div>
	  </div>
	</div>
      </div>

      <div class="sphinxsidebar">
	<div class="sphinxsidebarwrapper" style="float: left; margin-right: 0px; width: 172px;">


	    <a href="http://golem.fjfi.cvut.cz/shots/<?php echo $shot_no;?>"><img src="/_static/logos/golem.svg" alt="" width="80"></a>

		<h4>Diagnostics</h4>
		<ul>
		
				   <?php
    $dir='Diagnostics';
    system ("for i in `ls /golem/database/operation/shots/$shot_no/$dir/`; do echo '<li><a href=http://golem.fjfi.cvut.cz/shots/$shot_no/$dir/'\$i'/onstage.html>'\$i'</a></li>';done");
    ?>
		</ul>

<!--		<h4>Analysis</h4>

		<ul>
		   <?php
		   
//     $dir='analysis_wave_i';
//     system ("for i in `ls /golem/database/operation/shots/$shot_no/$dir/`; do echo '<li><a href=http://golem.fjfi.cvut.cz/shots/$shot_no/$dir/'\$i'/dev.html>'\$i'</a></li>';done");
//     echo "<li>--</li>";
//         $dir='analysis_wave_ii';
//     system ("for i in `ls /golem/database/operation/shots/$shot_no/$dir/`; do echo '<li><a href=http://golem.fjfi.cvut.cz/shots/$shot_no/$dir/'\$i'/dev.html>'\$i'</a></li>';done");
//     echo "<li>--</li>";
//         $dir='analysis_wave_iii';
//     system ("for i in `ls /golem/database/operation/shots/$shot_no/$dir/`; do echo '<li><a href=http://golem.fjfi.cvut.cz/shots/$shot_no/$dir/'\$i'/dev.html>'\$i'</a></li>';done");
// 
    
    
    ?>

		
 		</ul>-->


		<h3>Other</h3>
		<ul class="this-page-menu">
		
        
    
        
    
 <!--<li><a class="reference internal" href="http://golem.fjfi.cvut.cz/shots/<?php echo $shot_no;?>/Data.php">Data</a></li>-->
 <!--<li><a class="reference internal" href="http://golem.fjfi.cvut.cz/wiki/Bibliography.pdf">References</a></li>-->
 <!--<li><a class="reference internal" href="http://golem.fjfi.cvut.cz/shots/<?php echo $shot_no;?>/About.php">About</a></li>-->
 <li><a class="reference internal" href="http://golem.fjfi.cvut.cz/wiki/">Wiki</a></li>
 <li><?php echo $underconst; ?><a class="reference internal" href="Analysis/Homepage/bookmarks.html">"Showroom"</a>
  </li>
 <!--<li><a class="reference internal" href="http://golem.fjfi.cvut.cz/utils/">Utilities</a></li>-->

		</ul>

		<h4>Navigation</h4>
		<ul class="this-page-menu">
  
 <li>
    
    <a class="reference internal" href="http://golem.fjfi.cvut.cz/shots/<?php echo ($shot_no+1);?>">Next</a>
    </li>
  
 <li>
    
    <a class="reference internal" href="http://golem.fjfi.cvut.cz/shots/<?php echo ($shot_no-1);?>">Previous</a>
    </li>
<li>
    
    <a class="reference internal" href="http://golem.fjfi.cvut.cz/shots/0">Current</a>
    </li>

		</ul>

               <h3>Utilities</h3>
                <ul class="this-page-menu">
            <div id="searchbox" style="">

                 <li>
                    <form style="display:inline;" method="post" action="http://golem.fjfi.cvut.cz/shots/36503/" onsubmit="return getURL(this.shot.value)">

                    #<input size="3" type="text" name="shot" value="36503">
                   <input size="5" type="submit" name="Go" value="Go">

                    </form>
            <script type="text/javascript">$('#searchbox').show(0);</script>
            </li>
</div>
            <li><a class="reference internal" href="http://golem.fjfi.cvut.cz/plot/">Plot data</a>  </li>
             </ul>

	</div>
	</body></html>

<!--
SW develop	

cd /dev/shm/golem/ActualShot/Analysis/Homepage/;cp /golem/Dirigent/Analysis/Homepage/homepage.php .;source Homepage.sh ;DoShotHomepage ;ll

WholePage reconstruction incl Scan:
shotno=35681; cp -r /golem/svoboda/Dirigent/Analysis/Homepage/*.* /golem/database/operation/shots/$shotno/Analysis/Homepage/;cd /golem/database/operation/shots/$shotno/Analysis/Homepage;source Homepage.sh ;DoShotHomepage; cd $OLDPWD

Just index generation:
shotno=35993; cp -r /golem/svoboda/Dirigent/Analysis/Homepage/*.* /golem/database/operation/shots/$shotno/Analysis/Homepage/;cd /golem/database/operation/shots/$shotno/Analysis/Homepage;source Homepage.sh ;IndexGeneration; cd $OLDPWD



########## Dále nevím ...


shotno=33931; source ../../session.setup ;for i in $DASs; do echo Doing $das;cp -r /golem/svoboda/Dirigent/DASs/`dirname $i`/* /golem/database/operation/shots/$shotno/DASs/`basename $i`; cd /golem/database/operation/shots/$shotno/DASs/`basename $i`; source `basename $i`.sh; Web;cd - ;done;for i in $Diagnostics; do echo Doing $i;cp -r /golem/svoboda/Dirigent/Diagnostics/$i /golem/database/operation/shots/$shotno/Diagnostics; cd /golem/database/operation/shots/$shotno/Diagnostics/$i; source $i.sh; PostDischargeFinals;cd - ;done;cp -r /golem/svoboda/Dirigent/Analysis/Homepage /golem/database/operation/shots/$shotno/analysis_wave_0;cd /golem/database/operation/shots/$shotno/analysis_wave_0/Homepage;source Homepage.sh ;PostDischargeFinals


-->
	
